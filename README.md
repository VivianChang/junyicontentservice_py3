# JunyiContentService_py3

## Run docker
    $ docker pull gcr.io/junyiacademy/dev/localserver:1.0.18
    $ docker run -it \
                -v ~/src/junyicontentservice_py3:/src \
                -v ~/src/junyicontentservice:/py2_content_src  \
                -v ~/src/junyiacademy:/main_src \
                -v ~/work_resource/:/work_resource \
                -v ~/.config/:/.config \
                -p 8080:8080 -p 8000:8000 -p 8010:8010 \
                --name content_py3 \
                gcr.io/junyiacademy/dev/localserver:1.0.18 bash

## Run services

### 啟動 datastore emulator

`/work_resource/` 內必須有 `/work_resource/WEB-INF/appengine-generated/local_db.bin`

[下載 local_db.bin](https://drive.google.com/file/d/18Nr-00CdPzRuhv9xW2rWsFqDQiji429F)

    docker $ gcloud config set project junyiacademy
    docker $ gcloud beta emulators datastore start --data-dir=/work_resource --host-port=0.0.0.0:8085

確認是否有成功啟動 emulator

    docker $ curl localhost:8085

若回覆 `Ok` 表示有成功啟動

note: 需在 docker 內執行，除非你有把 8085 port 也開出來

### 單獨啟動 content service（推薦 content service 開發者使用）

另開一個 bash 啟動 flask，注意這樣會啟動在 8010 port

    $ docker exec -it content_py3 bash

設定環境變數

    docker $ $(gcloud beta emulators datastore env-init --data-dir=/work_resource)
    docker $ export

確認可以看到以下變數有成功設定

    declare -x DATASTORE_DATASET="junyiacademy"
    declare -x DATASTORE_EMULATOR_HOST="0.0.0.0:8085"
    declare -x DATASTORE_EMULATOR_HOST_PATH="0.0.0.0:8085/datastore"
    declare -x DATASTORE_HOST="http://0.0.0.0:8085"
    declare -x DATASTORE_PROJECT_ID="junyiacademy"

安裝 dependency // FIXME: should be included in docker image

    docker $ pip3 install -r requirements.txt

啟動 server

    docker $ cd /src
    docker $ python3 main.py

測試

    $ curl localhost:8010/api/content/hello

### 同時啟動多個服務，可以完整架起本地的均一教育平台
因為 junyiacademy 一個 content 需要的 api 還沒 merge, 所以請先 check out 到 `origin/inter-server-auth`

    $ cd src/junyiacademy
    $ git checkout --track remotes/origin/inter-server-auth

把 `junyiacademy/local_dispatch.yaml` 修改成以下內容

    dispatch:
    - url: "*/api/content/hello"
      service: contentfrontendpy3

    - url: "*/api/content/topicpage"
      service: contentfrontendpy3

    - url: "*/api/content/*"
      service: contentfrontend

接著啟動

    docker $ cd /
    docker $ dev_appserver.py \
                main_src/local_dispatch.yaml \
                main_src/app.yaml \
                src/app.yaml \
                py2_content_src/app.yaml \
                --support_datastore_emulator=true \
                --host=:: --admin_host=0.0.0.0 \
                -A junyiacademy

warmup 失敗 （`ERROR:main:Exception on /_ah/warmup [GET]`）很常見，只要 `contentfrontendpy3` 比 `default` 早就緒就會發生，可以忽略．

## Run tests

### Run all tests
    docker $ python3 tools/runtests.py

### Run single test file
    docker $ python3 tools/runtests.py content/internal/topic_test.py
    docker $ python3 tools/runtests.py content.internal.topic_test

### Run single test case
    docker $ python3 tools/runtests.py content.internal.topic_test.TestTopic
    docker $ python3 tools/runtests.py content.internal.topic_test.TestTopic.test_init_by_invalid_topic_id

### Run coverage
install package // FIXME: should be included in docker image

    docker $ pip3 install coverage

run

    docker $ coverage3 run tools/runtests.py content.internal.topic_test.TestTopic

report

    docker $ coverage3 report
    docker $ coverage3 html
