# -*- coding: utf-8 -*-
from werkzeug.local import Local, LocalManager
import inspect
import logging
import pdb #FIXME

_DUMMY_VALUE = object()
_LOCAL = Local()
LOCAL_MANAGER = LocalManager([_LOCAL])


def accepts_bust(target):
    sig = inspect.signature(target)
    for name in sig.parameters:
        if sig.parameters[name].kind == inspect.Parameter.VAR_KEYWORD:
            return True
        if name == 'bust_cache' and (
                sig.parameters[name].kind == inspect.Parameter.KEYWORD_ONLY or
                sig.parameters[name].kind == inspect.Parameter.POSITIONAL_OR_KEYWORD):
            return True
    return False


def cache():
    def decorator(target):
        key = "__request_cache_%s.%s__" % (target.__module__, target.__name__)

        def wrapper(*args, **kwargs):
            return request_cache_check_set_return(
                target,
                lambda *a, **kw: key,
                *args, **kwargs)

        return wrapper
    return decorator


def cache_with_key_fxn(key_fxn):
    def decorator(target):
        def wrapper(*args, **kwargs):
            return request_cache_check_set_return(target, key_fxn,
                                                  *args, **kwargs)
        return wrapper
    return decorator


def request_cache_check_set_return(
        target,
        key_fxn,
        *args,
        **kwargs):

    key = key_fxn(*args, **kwargs)

    bust_cache = False
    if "bust_cache" in kwargs:
        bust_cache = kwargs["bust_cache"]
        if not accepts_bust(target):
            # delete from kwargs so it's not passed to the target
            del kwargs["bust_cache"]

    if not bust_cache:
        # Access thread-local data once with getattr rather than twice in the
        # naive pattern -- if has(key): return get(key)
        result = getattr(_LOCAL, key, _DUMMY_VALUE)
        if result is not _DUMMY_VALUE:
            logging.info('request cache hit [%s]' % key)
            return result

    result = target(*args, **kwargs)

    # In case the key's value has been changed by target's execution
    key = key_fxn(*args, **kwargs)

    set_cache(key, result)

    return result




def has(key):
    return hasattr(_LOCAL, key)


def get_cache(key):
    return getattr(_LOCAL, key, None)


def set_cache(key, value, **kwargs):
    setattr(_LOCAL, key, value)


def flush():
    _LOCAL.__release_local__()


