# -*- coding: utf-8 -*-
import json
import os
import unittest
from unittest.mock import patch

import content.internal.internal
import main
from testutil.authutil import create_jaid


class TestXSFR(unittest.TestCase):
    def setUp(self):
        main.app.testing = True
        os.environ['FLASK_DEBUG'] = 'yes'
        self.client = main.app.test_client()

    def test_ok(self):
        fkey_value = 'xsrf_random_value'
        self.client.set_cookie('localhost', main.XSRF_COOKIE_KEY, fkey_value)
        r = self.client.get('/api/content/hello', headers={main.XSRF_HEADER_KEY: fkey_value})
        self.assertEqual(r.status_code, 200)

    def test_no_header(self):
        fkey_value = 'xsrf_random_value'
        self.client.set_cookie('localhost', main.XSRF_COOKIE_KEY, fkey_value)
        r = self.client.get('/api/content/hello')
        self.assertEqual(r.status_code, 400)

    def test_no_cookie(self):
        fkey_value = 'xsrf_random_value'
        r = self.client.get('/api/content/hello', headers={main.XSRF_HEADER_KEY: fkey_value})
        self.assertEqual(r.status_code, 400)

    def test_mismatch(self):
        fkey_value = 'xsrf_random_value'
        self.client.set_cookie('localhost', main.XSRF_COOKIE_KEY, fkey_value + 'lala')
        r = self.client.get('/api/content/hello', headers={main.XSRF_HEADER_KEY: fkey_value})
        self.assertEqual(r.status_code, 403)


class TestMainBase(unittest.TestCase):
    def setUp(self):
        main.app.testing = True
        os.environ['FLASK_DEBUG'] = 'yes'
        self.client = main.app.test_client()

    def fetch(self, url, method='get'):
        fkey_value = 'xsrf_random_value'
        self.client.set_cookie('localhost', main.XSRF_COOKIE_KEY, fkey_value)
        if method == 'post':
            return self.client.post(url, headers={main.XSRF_HEADER_KEY: fkey_value})
        return self.client.get(url, headers={main.XSRF_HEADER_KEY: fkey_value})


class TestAccessControl(TestMainBase):
    def test_moderator_required_forbidden(self):
        r = self.fetch('/api/content/topictree/default/edu_sheet/topic/create', method='post')
        self.assertEqual(r.status_code, 403)

    def test_moderator_required_forbidden_2(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid())
        r = self.fetch('/api/content/topictree/default/edu_sheet/topic/create', method='post')
        self.assertEqual(r.status_code, 403)

    """
    pass @moderator_required 進到 api 主程式就算 pass, 故意把版本放 default 是為了不要進到主程式更深的邏輯
    """

    def test_moderator_required_pass(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True))
        r = self.fetch('/api/content/topictree/default/edu_sheet/topic/create', method='post')
        self.assertEqual(r.status_code, 400)

    def test_moderator_required_pass_2(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid(developer=True))
        r = self.fetch('/api/content/topictree/default/edu_sheet/topic/create', method='post')
        self.assertEqual(r.status_code, 400)


class TestTopicPage(TestMainBase):
    def test_normal_user_ok(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid())
        r = self.fetch('/api/content/topicpage?topic_id=junyi-middle-school-biology')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        data = json.loads(r.data.decode('utf-8'))
        self.assertIsNotNone(data)
        self.assertEqual(data['title'], '國中生物')
        self.assertEqual(len(data['child']), 12)

    def test_normal_user_access_hidden_topic(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid())
        r = self.fetch('/api/content/topicpage?topic_id=khan-videos')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        data = json.loads(r.data.decode('utf-8'))
        self.assertIsNone(data)

    def test_developer_access_hidden_topic(self):
        self.client.set_cookie('localhost', 'JAID', create_jaid(moderator=True, developer=True))
        r = self.fetch('/api/content/topicpage?topic_id=khan-videos')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.headers['Content-Type'], 'application/json')
        data = json.loads(r.data.decode('utf-8'))
        self.assertIsNotNone(data)
        self.assertEqual(data['title'], 'khan videos [hidden]')
        self.assertEqual(len(data['child']), 29)

    def test_no_topic_id(self):
        r = self.fetch('/api/content/topicpage')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), 'Require [topic_id] GET param')

    def test_wrong_topic_id(self):
        r = self.fetch('/api/content/topicpage?topic_id=ba')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), 'No Topic for topic_id [ba] in default version')


class TestTopicTreeData(TestMainBase):
    def test_ok_current_default(self):
        """
        隨著測試資料庫有修改，回傳值會改變，所以這邊測的比較鬆
        """
        r = self.fetch('/api/content/topictreedata?casing=camel')
        self.assertEqual(r.status_code, 200)

        resp_headers = r.headers
        self.assertEqual(resp_headers['Content-Type'], 'application/json')
        self.assertTrue(int(resp_headers['Content-Length']) > 63500)

        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(len(resp_json.keys()), 6)
        self.assertEqual(resp_json['id'], 'root')
        self.assertEqual(resp_json['title'], 'The Root of All Knowledge')
        self.assertIsNotNone(resp_json['key'])
        self.assertListEqual(resp_json['tags'], [])
        self.assertFalse(resp_json['isContentTopic'])
        self.assertEqual(len(resp_json['childTopics']), 7)

    def test_ok_v350(self):
        """
        測試資料庫不太可能改到舊版的 topic version，所以這邊測的比較嚴
        """
        r = self.fetch('/api/content/topictreedata?casing=camel&topicversion=350')
        self.assertEqual(r.status_code, 200)

        resp_headers = r.headers
        self.assertEqual(resp_headers['Content-Type'], 'application/json')
        self.assertEqual(int(resp_headers['Content-Length']), 63594)

        resp_json = json.loads(r.data.decode('utf-8'))
        self.assertEqual(len(resp_json.keys()), 6)
        self.assertEqual(resp_json['id'], 'root')
        self.assertEqual(resp_json['title'], 'The Root of All Knowledge')
        self.assertEqual(resp_json['key'], 'ag5zfmp1bnlpYWNhZGVteXIzCxIFVG9waWMiKHZrMnlMa' +
                         'V9WemZMdG5BX25jYTM2V2NuN0p3STY2Q0E3VVRNbTZ6d0kM')
        self.assertListEqual(resp_json['tags'], [])
        self.assertFalse(resp_json['isContentTopic'])
        self.assertEqual(len(resp_json['childTopics']), 7)

        # subtree 抽查
        self.assertEqual(resp_json['childTopics'][3]['id'], 'arts-and-humanities')
        self.assertTrue(resp_json['childTopics'][3]['childTopics'][0]['childTopics'][0]['isContentTopic'])

    def test_not_camel(self):
        r = self.fetch('/api/content/topictreedata')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'), 'Error: Not support other casing!')

    def test_invalid_version(self):
        r = self.fetch('/api/content/topictreedata?casing=camel&topicversion=a')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'),
                         'Could not find content data of version a [version [a] not exists]')

    @patch('content.internal.topic.get_root_key')
    def test_invalid_root(self, get_root_key_patch):
        get_root_key_patch.return_value = content.internal.internal.get_client().key('Topic',
                                                                                     'wrong_root_key')
        r = self.fetch('/api/content/topictreedata?casing=camel&topicversion=350')
        self.assertEqual(r.status_code, 400)
        self.assertEqual(r.data.decode('utf-8'),
                         'Could not find content data of version 350 [root of version [350] not exists]')
