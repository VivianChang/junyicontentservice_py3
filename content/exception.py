# -*- coding: utf-8 -*-


class ContentNotExistsError(Exception):
    pass


class TopicExistsError(Exception):
    # 預期 topic 不存在，例如要建立新的，但實際上已經存在
    pass


class TopicKeyExistsError(Exception):
    # 預期 topic key 不存在，例如要建立新的，但實際上已經存在
    pass


class TopicIdError(Exception):
    pass
