# -*- coding: utf-8 -*-
import random
from google.cloud import datastore
from random import shuffle
import pdb #FIXME

def get_topic_entity(is_live=True, num_child_topic=0, num_child_each_content=0, child_is_live=None):
    ret = _get_topic_entity(is_live)
    children = []
    if child_is_live and num_child_topic != 0 and len(child_is_live) > num_child_topic:
        raise ValueError('len(child_is_live)[%d] should not be larger than num_child_topic [%d]' %
                         (len(child_is_live), num_child_topic))
    for i in range(num_child_topic):
        is_live = child_is_live[i % len(child_is_live)]
        children.append(_get_topic_entity(is_live))

    if child_is_live and num_child_each_content != 0 and len(child_is_live) > num_child_each_content:
        raise ValueError('len(child_is_live)[%d] should not be larger than num_child_each_content [%d]' %
                         (len(child_is_live), num_child_each_content))
    if child_is_live is None:
        child_is_live = [True]
    for i in range(num_child_each_content):
        is_live = child_is_live[i % len(child_is_live)]
        children.append(get_exam_entity(is_live))
        children.append(get_article_entity(is_live))
        children.append(get_url_entity())
        children.append(get_exercise_entity(is_live))
        children.append(get_video_entity())

    ret['child_keys'] = [child.key for child in children]
    shuffle(children)
    return ret, children

def _get_topic_entity(is_live=True):
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Topic', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    for item in ['title', 'standalone_title', 'id', 'extended_slug', 'description']:
        entity[item] = item + '_' + unique_suffix
    entity['tags'] = []
    entity['hide'] = not is_live
    entity['child_keys'] = []
    return entity

def get_exam_entity(is_live=True):
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Exam', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    entity['name'] = 'name_' + unique_suffix
    entity['content_list'] = ['dummy_content1', 'dummy_content2']
    entity['public'] = is_live
    entity['tags'] = []
    entity['large_screen_only'] = True
    return entity

def get_article_entity(is_live=True):
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Article', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    entity['name'] = 'name_' + unique_suffix
    entity['content_list'] = ['dummy_content1', 'dummy_content2']
    entity['public'] = is_live
    entity['tags'] = []
    entity['large_screen_only'] = True
    return entity

def get_url_entity():
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Url', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    for item in ['url', 'title', 'description']:
        entity[item] = item + '_' + unique_suffix
    return entity

def get_exercise_entity(is_live=True):
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Exercise', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    for item in ['name', 'pretty_display_name', 'description']:
        entity[item] = item + '_' + unique_suffix
    entity['live'] = is_live
    entity['is_quiz_exercise'] = True
    entity['large_screen_only'] = True
    return entity

def get_video_entity():
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('Video', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    entity = datastore.Entity(key)
    for item in ['youtube_id', 'url', 'title', 'description', 'keywords', 'readable_id']:
        entity[item] = item + '_' + unique_suffix
    entity['duration'] = 100
    return entity

def get_topic_version_entity():
    unique_suffix = str(random.randrange(10000, 99999))
    key = datastore.Key('TopicVersion', 'dummy_topic_id_' + unique_suffix, project='junyiacademy')
    return datastore.Entity(key)

