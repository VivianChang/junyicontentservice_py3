# -*- coding: utf-8 -*-
import urllib.parse
from .content_prototype import Content
import pdb #FIXME


class Exercise(Content):
    name: str
    pretty_display_name: str
    live: bool
    description: str
    is_quiz_exercise: bool
    large_screen_only: bool
    _property_from_entity = ['name', 'pretty_display_name', 'live', 'description', 'is_quiz_exercise',
                             'large_screen_only']

    @property
    def is_live(self):
        return self.live

    @property
    def is_teaching_material(self):
        return True

    @property
    def presented_title(self):
        if not self.pretty_display_name:
            import logging
            logging.error("exercise has no pretty_display_name. name: %s", self.name)
        if self.is_live:
            return self.pretty_display_name
        return self.pretty_display_name + ' [hidden]'

    @property
    def progress_id(self):
        return self.name

    @property
    def canonical_url(self):
        if not self._parent_topic:
            raise AttributeError('Require parent topic to get url')
        return "/%s/e/%s" % (self._parent_topic.extended_slug, urllib.parse.quote(self.name))


    def __init__(self, entity=None, parent_topic=None):
        super().__init__(entity, parent_topic)

    def info_to_topic_page(self):
        return {
            'url': self.canonical_url,
            'type': self.__class__.__name__,
            'title': self.presented_title,
            'description': self.description,
            'progress_id': self.progress_id,
            'id': self.name,
            'is_content': self.is_teaching_material,
            'is_quiz_exercise': self.is_quiz_exercise,
            'large_screen_only': self.large_screen_only
        }

