# -*- coding: utf-8 -*-
import urllib.parse
from .content_prototype import Content

class Video(Content):
    youtube_id: str
    url: str
    title: str
    description: str
    keywords: str
    duration: int
    readable_id: str

    _property_from_entity = ['youtube_id', 'url', 'title', 'description', 'keywords', 'duration', 'readable_id']

    @property
    def is_live(self):
        return True

    @property
    def is_teaching_material(self):
        return True

    @property
    def presented_title(self):
        if self.is_live:
            return self.title
        return self.title + ' [hidden]'

    @property
    def progress_id(self):
        return 'v' + str(self._entity.key.id)

    @property
    def canonical_url(self):
        if not self._parent_topic:
            raise AttributeError('Require parent topic to get url')
        return "/%s/v/%s" % (self._parent_topic.extended_slug, urllib.parse.quote(self.readable_id))


    def __init__(self, entity=None, parent_topic=None):
        super().__init__(entity, parent_topic)

    def info_to_topic_page(self):
        return {
            'url': self.canonical_url,
            'type': self.__class__.__name__,
            'title': self.presented_title,
            'description': self.description,
            'progress_id': self.progress_id,
            'id': self.readable_id,
            'is_content': self.is_teaching_material,
            'duration': self.duration,
            'duration_format': "%d:%02d" % (self.duration / 60, self.duration % 60)
        }

