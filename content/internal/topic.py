# -*- coding: utf-8 -*-
from typing import Callable
import base64
import datetime
import json
import logging
import os
import re

from google.cloud import datastore

import content.exception
import instance_cache
from . import internal
from .content_prototype import Content
from .cool_english import CoolEnglishBasicLearning


class Topic(Content):
    # === 基本欄位: Topic 資料庫欄位的子集合 ===
    title: str
    standalone_title: str
    id: str
    extended_slug: str  # #2975 fix 之後，all topic in published versions has extended_slug
    description: str
    tags: [str]
    hide: bool
    icon_src: str
    logo_src: str
    banner_src: str
    intro: str
    links: [] = []
    # === 關聯欄位：需要額外操作才能填入的欄位，使用前請檢查是否填入 ===
    children: [] = None
    ancestors: [] = None  # from parent to root, ancestors[0] is parent Topic, ancestors[-1] is root Topic

    # === Dependency injection 欄位 ===
    content_factory: Callable

    # === Private property ===
    # _property_from_entity 列舉的欄位會轉存到 Topic, 其餘還有
    # child_keys
    # parent_keys
    _property_from_entity = ['title', 'standalone_title', 'id', 'extended_slug', 'description',
                             'tags', 'hide', 'icon_src', 'logo_src', 'banner_src', 'intro']

    @property
    def presented_title(self):
        if self.is_live:
            return self.title
        return self.title + ' [hidden]'

    @property
    def has_children(self):
        return True if len(self._entity['child_keys']) > 0 else False

    @property
    def contains_teaching_material(self):
        return not all(child_key.kind == 'Topic' for child_key in self._entity['child_keys'])

    @property
    def is_live(self):
        return not self.hide

    @property
    def is_teaching_material(self):
        return False

    @property
    def topic_page_url(self):
        return '/%s' % self.extended_slug

    def __init__(self, *, entity, parent_topic=None):
        # live content tree 存在沒有 child_keys 的 Topic (不是分隔線)
        if 'child_keys' not in entity:
            entity['child_keys'] = []

        super().__init__(entity, parent_topic)
        if not self.tags:
            self.tags = []
        if 'links' in self._entity:
            self.links = json.loads(self._entity['links'])

    @classmethod
    def from_id(cls, *, topic_id: str):
        version = internal.get_default_version()
        entity = _get_by_id(topic_id, version)
        if entity is None:
            raise content.exception.ContentNotExistsError('No Topic for topic_id [%s] in default version' % topic_id)
        return cls(entity=entity)

    def _init_children(self):
        if self.content_factory is None:
            raise AttributeError("Require content_factory to initialize children.")

        if self.id in CoolEnglishBasicLearning.TopicData:
            entities = []
            for child_key, child_info in CoolEnglishBasicLearning.TopicData[self.id]['mapping'].items():
                key = datastore.Key('Iframe', child_key, project='junyiacademy')
                self._entity['child_keys'].append(key)
                entity = datastore.Entity(key)
                entity['id'] = child_key
                entity['title'] = child_info['title']
                entities.append(entity)
        else:
            entities = internal.datastore_ordered_get_multi(self._entity['child_keys'])

        self.children = [self.content_factory(entity=entity,
                                              parent_topic=self,
                                              _treat_topic_as_divider=self.contains_teaching_material)
                         for entity in entities]

    def _init_ancestors(self):
        client = internal.get_client()
        self.ancestors = client.get_multi(self._entity['ancestor_keys'])

    def info_to_topic_page(self, leading_topic=False, visible_content_selector=None):
        ret = {
            'topic_id': self.id,
            'title': self.presented_title,
            'description': self.description,
            'tags': self.tags,
            'icon_src': self.icon_src,
        }
        if leading_topic:
            assert visible_content_selector is not None

            if self.children is None:
                self._init_children()

            ret['breadcrumb'] = self.get_breadcrumb_data(visible_content_selector)
            ret['is_content_topic'] = self.contains_teaching_material
            ret['extended_slug'] = self.extended_slug
            ret['logo_src'] = self.logo_src
            ret['banner_src'] = self.banner_src
            ret['intro'] = self.intro
            ret['links'] = self.links
            ret['child'] = [t.info_to_topic_page() for t in self.children if visible_content_selector(t)]
            if self.contains_teaching_material:
                ret['child_video_count'] = len([v for v in self.children
                                                if v.__class__.__name__ == "Video" and visible_content_selector(v)])
                ret['child_exercise_count'] = len([v for v in self.children
                                                   if v.__class__.__name__ == "Exercise" and visible_content_selector(v)])

        return ret


    def subtree(self):
        if not self.has_children:
            return None
        if self.id == 'new-and-noteworthy' or (not self.is_live and self.id != 'root'):
            return None

        def odd_is_content_topic(t: Topic):
            return any(child_key.kind in ("Video", "Exercise", "Exam", "Article")
                       for child_key in t._entity['child_keys'])

        child_topic_entities = \
            internal.datastore_ordered_get_multi([k for k in self._entity['child_keys'] if k.kind == 'Topic'])
        child_topic_subtrees = []
        for child_topic_entity in child_topic_entities:
            child_topic_subtree = Topic(entity=child_topic_entity).subtree()
            if child_topic_subtree is not None:
                child_topic_subtrees.append(child_topic_subtree)

        return {
            'id': self.id,
            'key': self._entity.key.to_legacy_urlsafe(location_prefix='s~').decode('utf-8'),
            'title': self.title,
            'tags': self.tags,
            'isContentTopic': odd_is_content_topic(self),
            'childTopics': child_topic_subtrees
        }


    def get_parent(self):
        """
        不建議直接取用 self._parent_topic, self._parent_topic 可能未賦值
        :return: parent Topic
        """
        if self._parent_topic:
            return self._parent_topic

        # root has no parent
        if self.id == 'root':
            return None

        parent_entity = internal.get_client().get(self._entity['parent_keys'][0])
        self._parent_topic = Topic(entity=parent_entity)
        return self._parent_topic

    def get_breadcrumb_data(self, visible_content_selector):
        """
        =若整串路徑都可見=
        breadcrumb 是從 root 往下第一層 (e.g. 主題式）起算，不含 self 的路徑

        假設階層關係為：主題式 > 國小-數與量 > 整數 ，則
        1. self = 主題式: breadcrumb = []
        2. self = 國小-數與量: breadcrumb = [主題式]
        3. self = 整數: breadcrumb = [主題式, 國小-數與量]

        =若階層關係中有主題不可見=
        先展開可見版 breadcrumb．若 breadcrumb[N] 不可見，則回傳 breadcrumb[N+1:]
        假設階層關係為：主題式 > 國中-數學三百問 (不可見) > 數與量 > 整數與數線
        1. self = 主題式: breadcrumb = []
        2. self = 國中-數學三百問 (不可見): breadcrumb = [主題式]  （此案例應該不會發生，應該在外面擋住）
        3. self = 數與量: breadcrumb = []
        4. self = 整數與數線: breadcrumb = [數與量]

        :param visible_content_selector: 判定 Topic 是否可見的 function
        """
        parent = self.get_parent()
        if not parent or not visible_content_selector(parent):
            return []

        if parent.id == 'root':
            return []

        my_piece = {
            "topic_id": parent.id,
            "title": parent.title,
            "tags": parent.tags,
        }
        breadcrumb = parent.get_breadcrumb_data(visible_content_selector)
        breadcrumb.append(my_piece)
        return breadcrumb

    # Generates the slug path of this topic, including parents,
    # i.e. math/arithmetic/fractions
    def generate_extended_slug(self):
        self._init_ancestors()
        parent_ids = [topic['id'] for topic in self.ancestors]
        parent_ids.reverse()
        if len(parent_ids) > 1:
            if parent_ids[0] != "root":
                logging.error("first ancestor is not root. id: %s, parent_ids[0]: %s" % (self.id, parent_ids[0]))
            slug = "%s/%s" % ('/'.join(parent_ids[1:]), self.id)
        else:
            slug = self.id
        return slug

    def update_extended_slug(self, skip_put=False):
        self.extended_slug = self.generate_extended_slug()
        # FIXME: put
        # if not skip_put:
        #     self.put()
        return self.extended_slug


class Divider:
    """
    跟 content 同層的 Topic 在 topic page 顯示為分隔線，後台顯示為資料夾
    """
    _topic: Topic

    def __init__(self, entity, parent_topic=None):
        self._topic = Topic(entity=entity, parent_topic=parent_topic)

    @property
    def is_live(self):
        return self._topic.is_live

    @property
    def presented_title(self):
        if self.is_live:
            return self._topic.title
        return self._topic.title + ' [hidden]'

    def info_to_topic_page(self):
        return {
            'url': self._topic.topic_page_url,
            'type': 'Topic',
            'title': self.presented_title,
            'description': self._topic.description,
            'progress_id': None,
            'id': None,
            'is_content': self._topic.is_teaching_material,
            'has_children': self._topic.has_children,
            'topic_id': self._topic.id,
            'tags': self._topic.tags
        }


def get_query(ancestor=None):
    datastore_client = internal.get_client()
    if datastore_client.current_transaction or os.getenv('GAE_VERSION'):
        return datastore_client.query(kind='Topic', ancestor=ancestor)
    else:
        # ignore ancestor query if out of a txn
        # FIXME: this is a workaround for a bug in local datastore emulator
        # https://issuetracker.google.com/issues/151198233
        return datastore_client.query(kind='Topic')


def get_new_key_name():
    return base64.urlsafe_b64encode(os.urandom(30)).decode()


def get_key(key_name, parent):
    client = internal.get_client()
    return client.key('Topic', key_name, parent=parent)


def _get_by_id(topic_id, version, ancestor=None):
    query = get_query(ancestor)
    query.add_filter('id', '=', topic_id)
    query.add_filter('version', '=', version.key)
    fetched = list(query.fetch(1))
    if len(fetched) == 0:
        return None
    return fetched[0]


def get_root(version_id):
    version = internal.get_version(version_id)
    if not version:
        raise content.exception.ContentNotExistsError(u"version [%s] not exists"
                                                      % str(version_id))
    root_key = get_root_key(version)
    client = internal.get_client()
    root_entity = client.get(root_key)
    if not root_entity:
        raise content.exception.ContentNotExistsError(u"root of version [%i] not exists"
                                                      % version['number'])
    return Topic(entity=root_entity)


@instance_cache.cache_with_key_fxn(
    lambda version: "get_root_key_%d" % version['number'],
    available_seconds=0)
def get_root_key(version):
    return _get_by_id('root', version).key


def get_new_id(parent_id, title, version):
    potential_id = title.lower()
    potential_id = re.sub('[^a-z0-9]', '-', potential_id)
    # remove any trailing dashes (see issue 1140)
    potential_id = re.sub('-+$', '', potential_id)
    # remove any leading dashes (see issue 1526)
    potential_id = re.sub('^-+', '', potential_id)

    if not potential_id:
        potential_id = title

    if potential_id[0].isdigit():
        potential_id = parent_id + "-" + potential_id

    potential_id = "v" + str(version['number']) + "-" + potential_id

    number_to_add = 0
    current_id = potential_id
    root = get_root_key(version=version)
    while True:
        matching_topic = _get_by_id(current_id, version, root)
        if matching_topic is None:  # id is unique so use it and break out
            return current_id
        else:  # id is not unique so will have to go through loop again
            number_to_add += 1
            current_id = '%s-%s' % (potential_id, number_to_add)


def check_topic_id(topic_id):
    # Only accept a-z, -, _
    if not re.match(r'^[a-z0-9_-]+$', topic_id):
        raise content.exception.TopicIdError(u"代號 %s 存在不合法字元" % topic_id)


def create_topic(title, parent_id=None, topic_id=None, **kwargs):
    version = internal.get_edit_version()

    # generate new topic id if not available
    if not topic_id:
        topic_id = get_new_id(parent_id, title, version)
        logging.info("created a new id %s for %s", topic_id, title)

    check_topic_id(topic_id)

    # setting the root to be the parent so that inserts and deletes can be done in a transaction
    key_name = get_new_key_name()
    root_key = get_root_key(version=version)
    new_topic_key = get_key(key_name, root_key)

    # TODO: should do this in Topic class?
    new_topic_args = {
        'title': title,
        'standalone_title': title,
        'id': topic_id,
        'extended_slug': None,  # will be updated after we got parent entity in txn
        'description': None,
        'parent_keys': [],  # will be updated after we got parent entity in txn
        'ancestor_keys': [],  # will be updated after we got parent entity in txn
        'init_custom_stack': None,  # TODO: remove this?
        'version': version.key,
        'hide': False,
        'created_on': datetime.datetime.now(),
        'updated_on': datetime.datetime.now(),
        'last_edited_by': None,  # FIXME: current user
        'backup_timestamp': datetime.datetime.now(),
    }
    if 'standalone_title' in kwargs:
        new_topic_args['standalone_title'] = kwargs['standalone_title']
    if 'description' in kwargs:
        new_topic_args['description'] = kwargs['description']

    with internal.get_client().transaction():
        new_topic_entity = _create_topic_txn(root_key, version, parent_id, new_topic_key,
                                             new_topic_args)

    return new_topic_entity


def _create_topic_txn(root_key, version, parent_id, new_topic_key, new_topic_args):
    client = internal.get_client()
    topic_id = new_topic_args['id']

    # check parent
    parent_topic_entity = _get_by_id(parent_id, version, ancestor=root_key)
    if not parent_topic_entity:
        raise content.exception.ContentNotExistsError(u"預計擺放的母資料夾 [%s] 不存在" % parent_id)

    # check if topic already exists
    existing_topic_entity = _get_by_id(topic_id, version, ancestor=root_key)
    if existing_topic_entity:
        raise content.exception.TopicExistsError(u"已存在代號是 %s 的資料夾，該資料夾的標題是 [%s]" % (topic_id, existing_topic_entity['title']))

    # check if key already exists
    topic = client.get(new_topic_key)
    if topic is not None:
        raise content.exception.TopicKeyExistsError("auto generated random key name '%s' already exists. try it again." % new_topic_key.name)

    # create new entity
    new_topic_entity = datastore.Entity(key=new_topic_key)
    new_topic_entity.update(new_topic_args)

    # update ancestors
    parent_keys = [parent_topic_entity.key]
    ancestor_keys = parent_keys[:]
    ancestor_keys.extend(parent_topic_entity['ancestor_keys'])
    if len(parent_topic_entity['ancestor_keys']):
        extended_slug = "%s/%s" % (parent_topic_entity['extended_slug'], topic_id)
    else:
        extended_slug = topic_id
    new_topic_entity['parent_keys'] = parent_keys
    new_topic_entity['ancestor_keys'] = ancestor_keys
    new_topic_entity['extended_slug'] = extended_slug

    client.put(new_topic_entity)

    parents = client.get_multi(new_topic_entity['parent_keys'])
    for parent in parents:
        parent['child_keys'].append(new_topic_entity.key)
        client.put(parent)

    # FIXME: update version info
    # version.update()

    return new_topic_entity


def update_topic(validation_only, topic_id, **kwargs):
    version = internal.get_edit_version()

    new_topic_args = {}
    if 'title' in kwargs:
        new_topic_args['title'] = kwargs['title']
    if 'standalone_title' in kwargs:
        new_topic_args['standalone_title'] = kwargs['standalone_title']
    if 'description' in kwargs:
        new_topic_args['description'] = kwargs['description']
    if 'id' in kwargs:
        new_topic_args['id'] = kwargs['id']
        check_topic_id(new_topic_args['id'])

    root_key = get_root_key(version=version)
    with internal.get_client().transaction():
        topic_entity = _update_topic_txn(validation_only, root_key, version, topic_id,
                                         new_topic_args)

    return topic_entity


def _update_topic_txn(validation_only, root_key, version, topic_id, new_topic_args):
    # check if topic exists
    topic_entity = _get_by_id(topic_id, version, ancestor=root_key)
    if not topic_entity:
        raise content.exception.ContentNotExistsError(u"這個代號的資料夾不存在 %s" % topic_id)

    if validation_only:
        return None

    # update entity
    changed = False
    if "id" in new_topic_args and new_topic_args["id"] != topic_id:
        # check if topic already exists
        existing_topic_entity = _get_by_id(new_topic_args["id"], version, ancestor=root_key)
        if existing_topic_entity:
            raise content.exception.TopicExistsError(u"已存在代號是 %s 的資料夾，該資料夾的標題是 [%s]" % (new_topic_args["id"], existing_topic_entity['title']))
        changed = True

    for attr, value in new_topic_args.items():
        if topic_entity[attr] != value:
            topic_entity[attr] = value
            changed = True

    if changed:
        topic_entity['updated_on'] = datetime.datetime.now()
        topic_entity['last_edited_by'] = None  # FIXME: current user
        topic_entity['backup_timestamp'] = datetime.datetime.now()
        client = internal.get_client()
        client.put(topic_entity)
        # FIXME: update version info
        # version.update()

    return topic_entity
