# -*- coding: utf-8 -*-
import unittest
from unittest.mock import patch, Mock

from google.cloud import datastore

import content.exception
from content import exception
from content.factory import get_content_factory
from . import internal
from . import mock_entity
from . import topic


class TestTopic(unittest.TestCase):
    # TODO: cool english topic

    def test_update_extended_slug(self):
        t = topic.Topic(entity=topic._get_by_id('math', version=internal.get_default_version()))
        self.assertEqual(t.update_extended_slug(), 'math')
        t = topic.Topic(entity=topic._get_by_id('arithmetic', version=internal.get_default_version()))
        self.assertEqual(t.update_extended_slug(), 'math/arithmetic')
        t = topic.Topic(entity=topic._get_by_id('addition-subtraction', version=internal.get_default_version()))
        self.assertEqual(t.update_extended_slug(), 'math/arithmetic/addition-subtraction')

    @patch('content.internal.topic._get_by_id')
    @patch('content.internal.internal.get_default_version')
    def test_init_by_invalid_topic_id(self, default_version_patch, get_topic_patch):
        """
        模擬 topic_id 取不到 Topic entity
        """
        default_version_patch.return_value = None
        get_topic_patch.return_value = None
        self.assertRaises(exception.ContentNotExistsError, topic.Topic.from_id, topic_id='no-such-topic')
        self.assertTrue(default_version_patch.called)
        self.assertTrue(get_topic_patch.called)

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic._get_by_id')
    @patch('content.internal.internal.get_default_version')
    def test_init_by_topic_id_with_no_children(self, default_version_patch, get_topic_patch, get_children_patch):
        """
        模擬透過 topic_id 取到一個沒有 child_keys 的 Topic
        """
        default_version_patch.return_value = None
        get_topic_patch.return_value, get_children_patch.return_value = \
            mock_entity.get_topic_entity(is_live=False)
        del get_topic_patch.return_value['child_keys']
        t = topic.Topic.from_id(topic_id='useless_dummy_id')
        self.assertTrue(default_version_patch.called)
        self.assertTrue(get_topic_patch.called)
        self.assertIsNotNone(t._entity)
        self.assertFalse(t.contains_teaching_material)

        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        # 必須先設定 factory 才能呼叫 info_to_topic_page()
        self.assertRaises(AttributeError, t.info_to_topic_page, leading_topic=True,
                          visible_content_selector=lambda c: c.is_live)
        t.content_factory = get_content_factory()

        # 僅顯示 live 的 children
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertEqual(len(t.children), 0)
        self.assertIsNotNone(info)
        self.assertListEqual(list(info.keys()), ['topic_id', 'title', 'description', 'tags', 'icon_src', 'breadcrumb',
                                                 'is_content_topic', 'extended_slug', 'logo_src',
                                                 'banner_src', 'intro', 'links', 'child'])
        self.assertEqual(len(info['child']), 0)

        # 顯示全部 children
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: True)
        self.assertIsNotNone(info)
        self.assertListEqual(list(info.keys()), ['topic_id', 'title', 'description', 'tags', 'icon_src', 'breadcrumb',
                                                 'is_content_topic', 'extended_slug', 'logo_src',
                                                 'banner_src', 'intro', 'links', 'child'])
        self.assertEqual(len(info['child']), 0)

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic._get_by_id')
    @patch('content.internal.internal.get_default_version')
    def test_init_by_topic_id_contains_12_topics(self, default_version_patch, get_topic_patch, get_children_patch):
        """
        模擬透過 topic_id 取到一個含有 12 個子 topic (6 live) 的 Topic
        """
        default_version_patch.return_value = None
        get_topic_patch.return_value, get_children_patch.return_value = \
            mock_entity.get_topic_entity(num_child_topic=12, child_is_live=[True, False])
        t = topic.Topic.from_id(topic_id='useless_dummy_id')
        self.assertTrue(default_version_patch.called)
        self.assertTrue(get_topic_patch.called)
        self.assertIsNotNone(t._entity)
        self.assertFalse(t.contains_teaching_material)

        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        # 必須先設定 factory 才能呼叫 info_to_topic_page()
        self.assertRaises(AttributeError, t.info_to_topic_page, leading_topic=True,
                          visible_content_selector=lambda c: c.is_live)
        t.content_factory = get_content_factory()

        # 僅顯示 live 的 children
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertEqual(len(t.children), 12)
        self.assertIsNotNone(info)
        self.assertListEqual(list(info.keys()), ['topic_id', 'title', 'description', 'tags', 'icon_src', 'breadcrumb',
                                                 'is_content_topic', 'extended_slug', 'logo_src',
                                                 'banner_src', 'intro', 'links', 'child'])
        self.assertEqual(len(info['child']), 6)

        # 顯示全部 children
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: True)
        self.assertIsNotNone(info)
        self.assertListEqual(list(info.keys()), ['topic_id', 'title', 'description', 'tags', 'icon_src', 'breadcrumb',
                                                 'is_content_topic', 'extended_slug', 'logo_src',
                                                 'banner_src', 'intro', 'links', 'child'])
        self.assertEqual(len(info['child']), 12)

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.topic._get_by_id')
    @patch('content.internal.internal.get_default_version')
    def test_init_by_topic_id_contains_10_contents(self, default_version_patch, get_topic_patch, get_children_patch):
        """
        模擬透過 topic_id 取到一個含有 10 個 content (3 hide) 的 Topic
        """
        default_version_patch.return_value = None
        get_topic_patch.return_value, get_children_patch.return_value = \
            mock_entity.get_topic_entity(num_child_each_content=2, child_is_live=[True, False])
        t = topic.Topic.from_id(topic_id='mock_topic_id')
        self.assertTrue(t.contains_teaching_material)
        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        t.content_factory = get_content_factory()
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertEqual(t.children[0].__class__.__name__, 'Exam')
        self.assertEqual(t.children[1].__class__.__name__, 'Article')
        self.assertEqual(t.children[2].__class__.__name__, 'Url')
        self.assertEqual(t.children[3].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[4].__class__.__name__, 'Video')
        self.assertEqual(t.children[5].__class__.__name__, 'Exam')
        self.assertEqual(t.children[6].__class__.__name__, 'Article')
        self.assertEqual(t.children[7].__class__.__name__, 'Url')
        self.assertEqual(t.children[8].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[9].__class__.__name__, 'Video')
        self.assertTrue(t.children[0].is_live)
        self.assertTrue(t.children[1].is_live)
        self.assertTrue(t.children[2].is_live)
        self.assertTrue(t.children[3].is_live)
        self.assertTrue(t.children[4].is_live)
        self.assertFalse(t.children[5].is_live)
        self.assertFalse(t.children[6].is_live)
        self.assertTrue(t.children[7].is_live)  # Url is always live
        self.assertFalse(t.children[8].is_live)
        self.assertTrue(t.children[9].is_live)  # Video is always live
        self.assertIsNotNone(info)
        self.assertListEqual(list(info.keys()), ['topic_id', 'title', 'description', 'tags', 'icon_src',
                                                 'breadcrumb', 'is_content_topic', 'extended_slug',
                                                 'logo_src', 'banner_src', 'intro', 'links',
                                                 'child', 'child_video_count', 'child_exercise_count'])
        self.assertEqual(len(info['child']), 7)

    @patch.object(datastore.Client, 'get_multi')
    @patch('content.internal.internal.get_default_version')
    def test_init_by_entity_contains_10_contents_and_2_dividers(self, default_version_patch, get_children_patch):
        """
        模擬透過 topic_id 取到一個含有 10 個 content (3 hide) + 2 Divider(空 Topic) 的 Topic
        """
        default_version_patch.return_value = None
        topic_entity, children = \
            mock_entity.get_topic_entity(num_child_topic=2, num_child_each_content=2, child_is_live=[True, False])
        get_children_patch.return_value = children
        t = topic.Topic(entity=topic_entity)
        self.assertTrue(t.contains_teaching_material)
        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        t.content_factory = get_content_factory()
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertEqual(t.children[0].__class__.__name__, 'Divider')
        self.assertEqual(t.children[1].__class__.__name__, 'Divider')
        self.assertEqual(t.children[2].__class__.__name__, 'Exam')
        self.assertEqual(t.children[3].__class__.__name__, 'Article')
        self.assertEqual(t.children[4].__class__.__name__, 'Url')
        self.assertEqual(t.children[5].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[6].__class__.__name__, 'Video')
        self.assertEqual(t.children[7].__class__.__name__, 'Exam')
        self.assertEqual(t.children[8].__class__.__name__, 'Article')
        self.assertEqual(t.children[9].__class__.__name__, 'Url')
        self.assertEqual(t.children[10].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[11].__class__.__name__, 'Video')
        self.assertTrue(t.children[0].is_live)
        self.assertFalse(t.children[1].is_live)
        self.assertTrue(t.children[2].is_live)
        self.assertTrue(t.children[3].is_live)
        self.assertTrue(t.children[4].is_live)
        self.assertTrue(t.children[5].is_live)
        self.assertTrue(t.children[6].is_live)
        self.assertFalse(t.children[7].is_live)
        self.assertFalse(t.children[8].is_live)
        self.assertTrue(t.children[9].is_live)  # Url is always live
        self.assertFalse(t.children[10].is_live)
        self.assertTrue(t.children[11].is_live)  # Video is always live
        self.assertIsNotNone(info)
        self.assertListEqual(list(info.keys()), ['topic_id', 'title', 'description', 'tags', 'icon_src',
                                                 'breadcrumb', 'is_content_topic', 'extended_slug',
                                                 'logo_src', 'banner_src', 'intro', 'links',
                                                 'child', 'child_video_count', 'child_exercise_count'])
        self.assertEqual(len(info['child']), 8)

        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: True)
        self.assertEqual(t.children[0].__class__.__name__, 'Divider')
        self.assertEqual(t.children[1].__class__.__name__, 'Divider')
        self.assertEqual(t.children[2].__class__.__name__, 'Exam')
        self.assertEqual(t.children[3].__class__.__name__, 'Article')
        self.assertEqual(t.children[4].__class__.__name__, 'Url')
        self.assertEqual(t.children[5].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[6].__class__.__name__, 'Video')
        self.assertEqual(t.children[7].__class__.__name__, 'Exam')
        self.assertEqual(t.children[8].__class__.__name__, 'Article')
        self.assertEqual(t.children[9].__class__.__name__, 'Url')
        self.assertEqual(t.children[10].__class__.__name__, 'Exercise')
        self.assertEqual(t.children[11].__class__.__name__, 'Video')
        self.assertTrue(t.children[0].is_live)
        self.assertFalse(t.children[1].is_live)
        self.assertTrue(t.children[2].is_live)
        self.assertTrue(t.children[3].is_live)
        self.assertTrue(t.children[4].is_live)
        self.assertTrue(t.children[5].is_live)
        self.assertTrue(t.children[6].is_live)
        self.assertFalse(t.children[7].is_live)
        self.assertFalse(t.children[8].is_live)
        self.assertTrue(t.children[9].is_live)  # Url is always live
        self.assertFalse(t.children[10].is_live)
        self.assertTrue(t.children[11].is_live)  # Video is always live
        self.assertIsNotNone(info)
        self.assertListEqual(list(info.keys()), ['topic_id', 'title', 'description', 'tags', 'icon_src',
                                                 'breadcrumb', 'is_content_topic', 'extended_slug',
                                                 'logo_src', 'banner_src', 'intro', 'links',
                                                 'child', 'child_video_count', 'child_exercise_count'])
        self.assertEqual(len(info['child']), 12)

    @patch('content.internal.internal.get_default_version')
    def test_init_by_entity_cool_english_topic(self, default_version_patch):
        """
        模擬透過 topic_id 取到一個含有 10 個 content (3 hide) + 2 Divider(空 Topic) 的 Topic
        """
        default_version_patch.return_value = None
        topic_entity, children = mock_entity.get_topic_entity()
        topic_entity['id'] = 'ce-bl-grade-3'
        t = topic.Topic(entity=topic_entity)
        t.content_factory = get_content_factory()
        t.get_breadcrumb_data = Mock()
        t.get_breadcrumb_data.return_value = []
        info = t.info_to_topic_page(leading_topic=True, visible_content_selector=lambda c: c.is_live)
        self.assertTrue(t.contains_teaching_material)
        for i in range(12):
            self.assertEqual(t.children[i].__class__.__name__, 'Iframe')
        self.assertListEqual(list(info.keys()), ['topic_id', 'title', 'description', 'tags', 'icon_src',
                                                 'breadcrumb', 'is_content_topic', 'extended_slug',
                                                 'logo_src', 'banner_src', 'intro', 'links',
                                                 'child', 'child_video_count', 'child_exercise_count'])
        self.assertEqual(len(info['child']), 16)

    parent_id = 'math'
    new_id = 'new_topic_id'
    new_title = 'New Topic Title'
    new_desc = 'description of new topic'
    update_id = 'test_update_topic'
    update_title = 'Test Update Topic Title'
    update_desc = 'a fake topic to test update function'

    def _update_topic_ok(self, topic_id, new_topic_id, new_topic_title, new_topic_description):
        updated_topic = topic.update_topic(False, topic_id,
                                           id=new_topic_id,
                                           title=new_topic_title,
                                           standalone_title=new_topic_title,
                                           description=new_topic_description)
        self.assertEqual(updated_topic['id'], new_topic_id)
        self.assertEqual(updated_topic['title'], new_topic_title)
        self.assertEqual(updated_topic['description'], new_topic_description)

    @patch('google.cloud.datastore.Client.put')
    def test_update_topic_ok(self, put_patch):
        # validation only
        updated_topic = topic.update_topic(True, self.update_id)
        self.assertIsNone(updated_topic)
        self.assertFalse(put_patch.called)
        # not changed title and desc
        self._update_topic_ok(self.update_id, self.update_id,
                              self.update_title, self.update_desc)
        self.assertFalse(put_patch.called)
        # changed title and desc
        self._update_topic_ok(self.update_id, self.update_id + 'x',
                              self.update_title + 'x', self.update_desc + 'x')
        self.assertTrue(put_patch.called)

    def test_update_topic_fail_topic_id(self):
        # 測試不存在 topic
        self.assertRaisesRegex(content.exception.ContentNotExistsError, '這個代號的資料夾不存在 x',
                               topic.update_topic, True, 'x')
        # 測試已存在 id
        self.assertRaisesRegex(content.exception.TopicExistsError, '已存在代號是 math 的資料夾，該資料夾的標題是',
                               topic.update_topic, False, self.update_id, id='math')
        # 測試不合法 id
        self.assertRaisesRegex(content.exception.TopicIdError, '代號 id\+ 存在不合法字元',
                               topic.update_topic, False, self.update_id, id='id+')

    def _create_topic_ok(self, new_topic_title, parent_id, new_topic_id, new_topic_description):
        created_topic = topic.create_topic(new_topic_title, parent_id, new_topic_id,
                                           standalone_title=new_topic_title,
                                           description=new_topic_description)
        self.assertEqual(created_topic['title'], new_topic_title)
        if new_topic_id:
            self.assertEqual(created_topic['id'], new_topic_id)
        else:
            self.assertIsNotNone(created_topic['id'])
            new_topic_id = created_topic['id']
        self.assertEqual(created_topic['description'], new_topic_description)
        self.assertEqual(created_topic['version'], internal.get_edit_version().key)
        self.assertEqual(created_topic['extended_slug'], '%s/%s' % (parent_id, new_topic_id))
        # check parent
        client = internal.get_client()
        parent_topic_key = created_topic['parent_keys'][0]
        parent_topic = client.get(parent_topic_key)
        child_keys = parent_topic['child_keys']
        child_ids = [t['id'] for t in client.get_multi(child_keys)]
        self.assertIn(new_topic_id, child_ids)
        # revert changes
        parent_topic['child_keys'].remove(created_topic.key)
        client.put(parent_topic)
        client.delete(created_topic.key)
        # check revert
        parent_topic = client.get(parent_topic_key)
        child_keys = parent_topic['child_keys']
        child_ids = [t['id'] for t in client.get_multi(child_keys)]
        self.assertNotIn(new_topic_id, child_ids)

    def test_create_topic_ok(self):
        # happy path
        self._create_topic_ok(self.new_title, self.parent_id, self.new_id, self.new_desc)
        # get new topic id
        self._create_topic_ok(self.new_title, self.parent_id, None, self.new_desc)

    def test_create_topic_fail_topic_id(self):
        # 測試不存在 parent
        self.assertRaisesRegex(content.exception.ContentNotExistsError, '預計擺放的母資料夾 \[x] 不存在',
                               topic.create_topic, self.new_title, 'x', self.new_id)
        # 測試已存在 id
        self.assertRaisesRegex(content.exception.TopicExistsError, '已存在代號是 math 的資料夾，該資料夾的標題是',
                               topic.create_topic, self.new_title, self.parent_id, 'math')
        # 測試不合法 id
        self.assertRaisesRegex(content.exception.TopicIdError, '代號 id\+ 存在不合法字元',
                               topic.create_topic, self.new_title, self.parent_id, 'id+')

    @patch('content.internal.topic.get_key')
    def test_create_topic_fail_key(self, get_key):
        get_key.return_value = topic.get_root_key(version=internal.get_default_version())
        # 測試 new key name 碰撞
        self.assertRaisesRegex(content.exception.TopicKeyExistsError,
                               'auto generated random key name .* already exists. try it again.',
                               topic.create_topic, self.new_title, self.parent_id, self.new_id)

    @patch('content.internal.internal.get_default_version')
    def test_get_breadcrumb_data(self, get_default_version):
        normal_user_selector = lambda c: c.is_live
        super_user_selector = lambda c: True
        get_default_version.return_value = internal.get_version(version_id=350)
        under_hidden_parent_topic = topic.Topic.from_id(topic_id='khan-fractions')
        self.assertListEqual([], under_hidden_parent_topic.get_breadcrumb_data(normal_user_selector))
        self.assertListEqual([{'topic_id': 'khan-videos', 'title': 'khan videos', 'tags': []}],
                             under_hidden_parent_topic.get_breadcrumb_data(super_user_selector))

        root = topic.Topic.from_id(topic_id='root')
        self.assertListEqual(root.get_breadcrumb_data(super_user_selector), [])
        self.assertListEqual(root.get_breadcrumb_data(normal_user_selector), [])


class TestTopicUtil(unittest.TestCase):
    @patch.object(datastore.Query, 'fetch')
    def test_get_by_id_no_result(self, fetch_patch):
        fetch_patch.return_value = []
        self.assertIsNone(topic._get_by_id('123', mock_entity.get_topic_version_entity()))

# TODO: len(fetched) == 0
