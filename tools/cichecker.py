#!/usr/bin/env python
"""
check pylint,coverage output for CI
"""



import re
import os
import sys
import abc
import json
import argparse
import logging
import errno
import subprocess
import six
import astroid
import collections

try:
    import coverage
except ImportError:
    coverage = None

_SRC = os.path.dirname(os.path.dirname(__file__))

SKIP_DIRS = (
        'third_party',
        'deploy',
        'cicd_py3',
        'gae_mini_profiler',
        'data_export',
        'offline'
        )

def fix_sys_path():
    sys.path.extend([_SRC, os.path.join(_SRC, 'third_party'), '/usr/local/google_appengine'])

    import dev_appserver
    dev_appserver.fix_sys_path()

def source_walk(root, relative=_SRC, skip_test=True):
    if len(root) > 1:
        root = root.rstrip(os.sep)
    if relative is True:
        relative = root
    elif len(relative) > 1:
        relative = relative.rstrip(os.sep)

    for dname, dlist, flist in os.walk(root):
        logging.debug(dname)

        for i, d in enumerate(dlist):
            if d in SKIP_DIRS:
                logging.debug('skip: %s', d)
                del dlist[i]

        for fname in flist:
            if os.path.splitext(fname)[1] != '.py':
                continue
            if skip_test and fname.endswith('_test.py'):
                continue

            path = os.path.join(dname, fname)

            if relative:
                # return a relative path from the root
                if path.startswith(relative + os.sep):
                    path = path[len(relative) + 1:]

            logging.info('> %s', path)
            yield path

def colored(msg, fg=None, bg=None, light=False):
    COLOR_CHAR = 'krgybpcw'
    COLOR_NAME = ('black', 'red', 'green', 'yellow', 'blue', 'purple', 'cyan', 'white')
    def _code(c):
        if isinstance(c, int):
            return c % 10
        elif len(c) == 1:
            return COLOR_CHAR.find(c)
        return COLOR_NAME.index(c)

    code = []
    if light:
        code.append('1')
    if fg is not None:
        code.append(str(30 + _code(fg)))
    if bg is not None:
        code.append(str(40 + _code(bg)))

    if code:
        return '\033[{}m{}\033[0m'.format(';'.join(code), msg)
    return '{}'.format(msg)

class Selector(object):
    def __init__(self, values='', sep=None, select_all=False):
        self._select_all = select_all
        self._set = set(values.split(sep)) if sep else values

    def __contains__(self, key):
        if self._select_all:
            return True
        if isinstance(self._set, collections.Callable):
            return self._set(key)
        return key in self._set

SELECT_ALL = Selector(select_all=True)
ALL_PYTHON_CODES = Selector(lambda x: x.endswith('.py'))

class GitUtil(object):
    GIT_BIN = '/usr/bin/git'
    BRANCHES = ('origin/master')
    #BRANCHES = ('origin/master', 'origin/staging')
    HEAD = 'HEAD'

    @classmethod
    def merge_base(cls, commit=HEAD, refs=BRANCHES):
        """get the merge base of commits

        * git merge-base ${1:-HEAD} ${2:-origin/master}
        * diff -u <(git rev-list --first-parent ${2:-HEAD}) \
                <(git rev-list --first-parent ${1:-origin/master})

        ref: https://stackoverflow.com/questions/1527234/finding-a-branch-point-with-git
        """

        cmd = [cls.GIT_BIN, 'merge-base', commit]
        if isinstance(refs, str):
            cmd.append(refs)
        else:
            cmd.extend(refs)
        result = subprocess.check_output(cmd).strip()
        logging.info('merge-base %s %s = %s', commit, refs, result)
        return result

    class DiffParser(object):
        DIFF_LINE = 'diff --git a/'

        def __init__(self, diff_input, no_deleted=False):
            self._diff_line_it = enumerate(diff_input, start=1)
            self.curr_diff_line = (None, '')
            self.drain_lines()
            self.no_deleted = no_deleted

        def next_diff_line(self):
            self.curr_diff_line = next(self._diff_line_it)
            if self.curr_diff_line[1].endswith(os.linesep):
                self.curr_diff_line = (self.curr_diff_line[0], self.curr_diff_line[1][:-1])
            logging.debug(self.curr_diff_line)
            return self.curr_diff_line

        def drain_lines(self):
            _, line = self.curr_diff_line
            try:
                while True:
                    if line.startswith(self.DIFF_LINE):
                        break
                    _, line = self.next_diff_line()
            except StopIteration:
                pass

        def __iter__(self):
            return self

        def __next__(self):
            self.drain_lines()
            fdiff = GitUtil.DiffFile(self)
            if self.no_deleted and fdiff.status == fdiff.DELETED:
                return next(self)
            return fdiff

    class DiffFile(object):
        DIFF_FILE_RE = re.compile(r'(---|\+\+\+) (/dev/null|[ab]/(.*))')
        DIFF_LINENO_RE = re.compile(r'@@ -(\d+),\d+ \+(\d+),\d+ @@.*')
        ADDED, DELETED, MODIFIED = ('A', 'D', 'M')

        origin_fname = None
        fname = None
        status = None
        headers = None
        lineno = (None, None)
        lineno_line = None

        def __init__(self, parser):
            self._parser = parser

            i, self.headers = parser.curr_diff_line
            def next_header_line():
                i, line = parser.next_diff_line()
                self.headers += line
                return i, line

            # match optional line "new file mode ..." or "deleted ..."
            i, line = next_header_line()
            if line.startswith('new file mode ') or line.startswith('deleted '):
                i, line = next_header_line()
            # match line "index ..."
            if not line.startswith('index '):
                raise RuntimeError('parse #%d:[%s] as index failed' % (i, line))

            # match line "--- a/..."
            i, line = next_header_line()
            matched = self.DIFF_FILE_RE.match(line)
            if not matched:
                raise RuntimeError('parse #%d:[%s] as a/* failed' % (i, line))
            ftype, _, self.origin_fname = matched.groups()
            if ftype != '---':
                raise RuntimeError('parse #%d:[%s] as a/* failed' % (i, line))

            # match line "+++ b/..."
            i, line = next_header_line()
            matched = self.DIFF_FILE_RE.match(line)
            if not matched:
                raise RuntimeError('parse #%d:[%s] as b/* failed' % (i, line))
            ftype, _, self.fname = matched.groups()
            if ftype != '+++':
                raise RuntimeError('parse #%d:[%s] as a/* failed' % (i, line))

            if self.origin_fname is None:
                assert self.fname is not None
                self.status = self.ADDED
            elif self.fname is None:
                self.status = self.DELETED
            else:
                self.status = self.MODIFIED

        def __iter__(self):
            return self

        def __next__(self):
            i, line = self._parser.next_diff_line()

            if line.startswith('@@'):
                # match line "@@ -...,... +...,... @@ ..."
                matched = self.DIFF_LINENO_RE.match(line)
                if not matched:
                    raise RuntimeError('parse #%d:[%s] as @@ line failed' % (i, line))
                self.lineno_line = line
                self.lineno = tuple(int(x) - 1 for x in matched.groups())
                return next(self)
            elif line.startswith('\\'):
                # ex. "\ No newline at end of file"
                return next(self)
            elif line[0] == ' ':
                self.lineno = tuple(x + 1 for x in self.lineno)
                return GitUtil.DiffLine(self, line)
            elif line[0] == '-':
                self.lineno = (self.lineno[0] + 1, self.lineno[1])
                return GitUtil.DiffLine(self, line)
            elif line[0] == '+':
                self.lineno = (self.lineno[0], self.lineno[1] + 1)
                return GitUtil.DiffLine(self, line)
            elif line.startswith('diff --git a/'):
                raise StopIteration
            else:
                raise RuntimeError('parse #%d:[%s] failed, unknown line type' % (i, line))

    class DiffLine(object):
        ADDED, DELETED, NOCHANGE = ('+', '-', ' ')

        def __init__(self, diff_file, line):
            self.diff_file = diff_file
            self.origin_lineno, self.lineno = diff_file.lineno
            self.status = line[0]
            self.text = line[1:]

        def __str__(self):
            return self.text

    @classmethod
    def diff(cls, commit1, commit2=None):
        """ get git diff of two commit """
        logging.debug('git diff %s %s', commit1, commit2)
        cmd = [cls.GIT_BIN, 'diff', commit1]
        if commit2 is not None:
            cmd.append(commit2)
        out = subprocess.check_output(cmd)
        return out.splitlines()

    @classmethod
    def diff_files(cls, commit1, commit2=None):
        """ get git diff files of two commit """
        logging.debug('git diff --name-status %s %s', commit1, commit2)
        cmd = [cls.GIT_BIN, 'diff', '--name-status', commit1]
        if commit2 is not None:
            cmd.append(commit2)
        out = subprocess.check_output(cmd)
        return out.splitlines()

class PylintUtil(object):
    MAYBE_FALSE_ALARM = tuple()
    """
    set to ignore some lint message, e.g.
    MAYBE_FALSE_ALARM = ({
            '_category_': 'cant import',
            'symbol': 'import-error',
            'msg': ("'js_css_version'", "'npm'", "'secrets_dev'", "'mod.submod1.submod2'"),
        },)
    """

    AS_ERROR_SYMBOLS = set((
        'abstract-method',
        'super-init-not-called',
        'duplicate-key',
        #'global-statement',
        #'redefined-outer-name',
        #'redefined-builtin',
        #'unused-variable',
        #'unused-argument',
        #'dangerous-default-value',
        #'protected-access',
        #'arguments-differ',
        #'relative-import',
        #'unused-import',
        #'logging-not-lazy',
        #'bare-except',
        #'broad-except',
        #'inconsistent-return-statements',
        ))

    MSG_TYPES = 'FEWRCI'
    CATEGORY_MAP = {
        'F': ('[F]atal', 'red'),
        'E': ('[E]rror', 'red'),
        'W': ('[W]arning', 'yellow'),
        'R': ('[R]efactor', 'yellow'),
        'C': ('[C]onvention', 'yellow'),
        'I': ('[I]', 'yellow'),
        }

    @classmethod
    def parse(cls, stream):
        """scan stream to emit LintMessage"""
        report_empty = True
        for line in stream:
            if line == 'Report' or line.startswith('-'*30):
                report_empty = False
                break
            msg = cls.LintMessage.parse(line)
            if msg is None:
                continue

            report_empty = False
            yield msg

        if report_empty:
            yield cls.LintMessage('cichecker.py', 114, 13, 'F',
                    'parse-error', 'parse pylint result failed', 'cichecker', 'LintMessage')

    class LintMessage(object):
        '''
        --------|---------|-------------------------------
        path    | path    | relative path to the file
        (NA)    | abspath | absolute path to the file
        line    | line    | line number
        column  | column  | column number
        module  | module  | module name
        obj     | obj     | object within the module (if any)
        msg     | msg     | text of the message
        (NA)    | msg_id  | the message code (eg. I0011)
        symbol  | symbol  | symbolic name of the message (eg. locally-disabled)
        category| C       | one letter indication of the message category
        (NA)    | category| fullname of the message category

        https://docs.pylint.org/en/1.6.0/output.html
        '''
        # pylint: disable=too-many-instance-attributes,too-many-arguments
        FORMAT = '{path}:{line}:{column}:{module}.{obj}: [{category}] {symbol} - {msg}'
        TEXT_RE = re.compile(r'^([^:]+):([0-9]+):([0-9]+):([^:]*?)\.([^:]*):'
                             r' \[(\w)\] ((\w|-)+) - (.*)$')

        def __init__(self, path, line, column, category, symbol, msg, module=None, obj=None):
            self.path = path
            self.line = int(line)
            self.column = int(column)
            self.category = category
            self.symbol = symbol
            self.msg = msg
            self.module = module or ''
            self.obj = obj or ''
            self.where = '{}:{}'.format(self.module, self.obj)

        def __str__(self):
            return self.FORMAT.format(**vars(self))

        @classmethod
        def parse(cls, text):
            matched = cls.TEXT_RE.match(text)
            if not matched:
                return None

            path, linenum, col, module, obj, category, symbol, dummy, msg = matched.groups()
            return cls(path, linenum, col, category, symbol, msg, module, obj)

        def match(self, match_spec):
            def exactly_match(content, pattern):
                if isinstance(pattern, (int, str)):
                    return pattern == content
                elif isinstance(pattern, tuple):
                    return content in pattern
                return re.match(pattern, content)

            def partial_match(content, pattern):
                if isinstance(pattern, int):
                    # no partial match for int
                    return pattern == content
                elif isinstance(pattern, str):
                    return pattern in content
                elif isinstance(pattern, tuple):
                    return any([x in content for x in pattern])
                return re.search(pattern, content)

            pname = match_spec.get('_category_', match_spec.get('where', ''))
            for key in ('where', 'symbol', 'category', 'path', 'module', 'obj'):
                if key not in match_spec:
                    continue
                logging.debug('  [%s] exactly check "%s"', pname, key)
                content = getattr(self, key)
                if not exactly_match(content, match_spec[key]):
                    return False

            for key in ('msg', ):
                if key not in match_spec:
                    continue
                logging.debug('  [%s] partial check "%s"', pname, key)
                content = getattr(self, key)
                if not partial_match(content, match_spec[key]):
                    return False

            logging.debug('* [%s] skiped by %s', pname, match_spec)
            return True

        def is_error(self):
            for match_spec in PylintUtil.MAYBE_FALSE_ALARM:
                if self.match(match_spec):
                    return False
            if self.category in ('E', 'F') or self.symbol in PylintUtil.AS_ERROR_SYMBOLS:
                return True
            return False

    class LintStatistics(object):
        def __init__(self):
            MSG_TYPES = PylintUtil.MSG_TYPES
            self.symbol = {}
            self.skip = 0
            self.errors = 0
            self.category = dict([(x, 0) for x in MSG_TYPES])
            self.cat_detail = dict([(x, set()) for x in MSG_TYPES])

        def scan(self, msg):
            logging.debug('%s', msg)
            if msg.is_error():
                self.errors += 1
            else:
                self.skip += 1
            self.symbol[msg.symbol] = self.symbol.get(msg.symbol, 0) + 1
            self.category[msg.category] += 1
            self.cat_detail[msg.category].add(msg.symbol)

        @classmethod
        def _sort_counts(cls, counts, top=11, reverse=True):
            ''' sort iterable tuple with the form [('key', count),...] '''
            if top is None:
                return sorted(counts, key=lambda x: x[1], reverse=reverse)
            counts = list(sorted(counts, key=lambda x: x[1], reverse=reverse))
            other = 0
            for x in counts[top:]:
                other += x[1]
            if other:
                return counts[:top] + [(colored('<others>', fg='k'), other)]
            return counts[:top]

        @classmethod
        def _dump_counts(cls, output, counts):
            line_length = 0
            for x in counts:
                msg = '  {} {}'.format(x[0], colored(x[1], fg='k'))
                msg = '{:<45}'.format(msg)
                if line_length > 70:
                    line_length = 0
                    print('', file=output)
                # the length include color is not real, but don't care
                line_length += len(msg)
                print(msg, end='', file=output)
            if line_length > 0:
                print('', file=output)

        def summary(self, output):
            for category in PylintUtil.MSG_TYPES:
                if self.category.get(category, 0) == 0:
                    continue
                print(colored('{} ({}): '.format(
                            PylintUtil.CATEGORY_MAP[category][0],
                            self.category.get(category, 0)
                        ), fg=PylintUtil.CATEGORY_MAP[category][1]), file=output)

                self._dump_counts(output, self._sort_counts([(x, self.symbol[x]) \
                        for x in self.cat_detail[category]], top=11))

            #total = sum(self.symbol.itervalues())
            print(colored('Errors: {}'.format(self.errors),
                bg=('red' if self.errors else 'green')), file=output)

            return not self.errors

        def top_messages(self, output):
            total = self.skip + self.errors
            print(colored('top messages ({})'.format(total), fg='cyan'), file=output)
            self._dump_counts(output, self._sort_counts(list(self.symbol.items())))

    @classmethod
    def build_index(cls, pylint_result, files=SELECT_ALL):
        try:
            with open(pylint_result, 'r') as stream:
                index = cls.LintIndex()
                for msg in PylintUtil.parse(stream):
                    if msg.path not in files:
                        continue
                    index.scan(msg)
                return index
        except IOError:
            logging.warning('%s: pylint result not ready', pylint_result)
            return cls.LintIndex(none=True)

    @classmethod
    def build_index_by_diff(cls, pylint_result, git_diff, files=SELECT_ALL):
        try:
            line_info = {}
            for diff in GitUtil.DiffParser(git_diff, no_deleted=True):
                if diff.fname not in files:
                    continue
                line_info[diff.fname] = set()
                for line in diff:
                    line_info[diff.fname].add(line.lineno)

            with open(pylint_result, 'r') as stream:
                index = cls.LintIndex()
                for msg in PylintUtil.parse(stream):
                    if msg.path not in line_info:
                        continue
                    if msg.line not in line_info[msg.path]:
                        continue
                    index.scan(msg)
                return index
        except IOError:
            logging.warning('%s: pylint result not ready', pylint_result)
            return cls.LintIndex(none=True)

    class LintIndex(object):
        def __init__(self, none=False):
            self._lines = None if none else {}

        def scan(self, msg):
            if msg.path not in self._lines:
                self._lines[msg.path] = {'_errors': 0}
            self._lines[msg.path][msg.line] = msg
            if msg.is_error():
                self._lines[msg.path]['_errors'] += 1
            if msg.symbol == 'parse-error':
                self._lines = None

        def __bool__(self):
            return self._lines is not None

        def count(self, fname):
            if self._lines is None:
                return None
            return len(self._lines.get(fname, {}))

        def count_error(self, fname):
            if self._lines is None:
                return None
            return self._lines.get(fname, {}).get('_errors', 0)

        def lint(self, fname, lineno):
            cls = PylintUtil.LintMessage
            if self._lines is None:
                return cls(fname, lineno, 0, '?', '???', '???', '?', '?')
            if fname not in self._lines:
                return cls(fname, lineno, 0, ' ', '', '', '?', '?')
            if lineno not in self._lines[fname]:
                return cls(fname, lineno, 0, ' ', '', '', '?', '?')
            return self._lines[fname][lineno]

class CoverageUtil(object):
    def __init__(self):
        if coverage:
            # pylint: disable=no-member
            self._coverage = coverage.Coverage(config_file='.coveragerc')
            self._coverage.load()
        else:
            self._coverage = None
        self._cache_analysis = None
        self._cache = {}

    def analyze(self, fname, src=None):
        """ return the missing line numbers (a set() object) or None if no record """
        if self._coverage is None:
            return self.AnalysisUtil(None, covered='?')

        if os.path.isabs(fname):
            fpath = fname
        elif src is not None:
            fpath = os.path.join(src, fname)
        else:
            fpath = os.path.abspath(fname)

        if fname not in ALL_PYTHON_CODES:
            logging.info('get coverage info for file: %s -> not .py', fpath)
            return self.AnalysisUtil(None, covered=' ')

        if hasattr(self._coverage, 'omit_match') and self._coverage.omit_match.match(fpath):
            logging.info('get coverage info for file: %s -> omit', fpath)
            return self.AnalysisUtil(None, covered=' ')
        logging.info('get coverage info for file: %s', fpath)
        return self.AnalysisUtil(self._coverage._analyze(fpath)) # pylint: disable=protected-access

    def check_diff(self, stream, files=SELECT_ALL):
        missings = {}
        for diff in GitUtil.DiffParser(stream, no_deleted=True):
            if diff.fname not in files:
                continue
            analysis = self.analyze(diff.fname)
            if not analysis:
                continue

            assert diff.fname not in missings
            missings[diff.fname] = self.check_diff_file(diff, analysis)

        return missings

    @classmethod
    def mark_is_covered(cls, mark):
        return cls.AnalysisUtil.mark_is_covered(mark)

    @classmethod
    def check_diff_file(cls, diff, analysis):
        missing = {'rm': 0, 'add': 0, 'all_add': 0, 'all_rm': 0}

        pstatus, first_rm_line = (' ', None)
        for line in diff:
            if line.status == line.DELETED:
                if pstatus != line.DELETED:
                    first_rm_line = line
            elif line.status == line.NOCHANGE:
                if pstatus == line.DELETED and first_rm_line is not None:
                    if not analysis.is_covered(first_rm_line):
                        # FIXME maybe false alarm/missing
                        # privous line may not be in the same block
                        missing['rm'] += 1
                    first_rm_line = None
                    missing['all_rm'] += 1
            elif line.status == line.ADDED:
                if pstatus == line.DELETED:
                    first_rm_line = None
                if not analysis.is_covered(line):
                    missing['add'] += 1
                missing['all_add'] += 1
            pstatus = line.status
        return missing

    class AnalysisUtil(object):
        BLANK_RE = re.compile(r"\s*(#|$)")
        ELSE_RE = re.compile(r"\s*else\s*:\s*(#|$)")

        COVERED, IGNORED, MISSED, EXCLUDED, UNKNOWN = ('>', ' ', '!', '-', '?')

        def __init__(self, analysis, covered=None):
            self._analysis = analysis
            self._default_covered = covered
            if analysis:
                self.statements = sorted(analysis.statements)
                self.missing = sorted(analysis.missing)
                self.excluded = sorted(analysis.excluded)
            self.i, self.j = (0, 0)

        def __bool__(self):
            return self._analysis is not None

        @staticmethod
        def _find_right_index(sorted_list, value, i=0):
            """linear search, because we will cache the last index i"""
            assert i >= 0 and i <= len(sorted_list)
            while i > 0 and sorted_list[i-1] > value:
                i -= 1
            while i < len(sorted_list) and sorted_list[i] < value:
                i += 1
            return i

        def covered(self, lineno, line='dummy'):
            if self._analysis is None:
                return self._default_covered

            if isinstance(lineno, GitUtil.DiffLine):
                lineno, line = lineno.lineno, lineno.text

            statements, missing, excluded, i, j = (
                    self.statements, self.missing, self.excluded, self.i, self.j)

            i = self.i = self._find_right_index(statements, lineno, self.i)
            j = self.j = self._find_right_index(missing, lineno, self.j)

            if i < len(statements) and statements[i] == lineno:
                covered = j >= len(missing) or missing[j] > lineno
            elif i == 0:
                covered = True
            else: # lookup previous statement
                covered = statements[i-1] not in missing

            if self.BLANK_RE.match(line):
                mark = self.IGNORED
            elif self.ELSE_RE.match(line):
                # Special logic for lines containing only 'else:'.
                if i >= len(statements) and j >= len(missing):
                    mark = self.MISSED
                elif i >= len(statements) or j >= len(missing):
                    mark = self.COVERED
                elif statements[i] == missing[j]:
                    mark = self.MISSED
                else:
                    mark = self.COVERED
            elif lineno in excluded:
                mark = self.EXCLUDED
            elif covered:
                mark = self.COVERED
            else:
                mark = self.MISSED

            if 'raise NotImplementedError' in line:
                # FIXME ignore raise NotImplementedError coverage?
                mark = self.EXCLUDED
            return mark

        @classmethod
        def mark_is_covered(cls, mark):
            return mark not in (cls.MISSED, cls.UNKNOWN)

        def is_covered(self, lineno, line='dummy'):
            return self.mark_is_covered(self.covered(lineno, line))

        def coverage(self):
            if self._analysis is None:
                if self.mark_is_covered(self._default_covered):
                    return 100.0
                return 0.0
            covered = len(self.statements) - len(self.missing)
            return 100.0 * covered / len(self.statements)

class ActionRunner(object):
    all_actions = []

    def __init__(self):
        super(ActionRunner, self).__init__()
        self.parser = self.init_parsers()

    def init_parsers(self):
        def doc(text):
            var = dict(
                    src=__file__
                    )
            ret = ''
            for line in text.splitlines():
                if line.startswith(' '*8):
                    line = line[8:]
                ret += line + os.linesep
            return ret.format(**var)
        parser = argparse.ArgumentParser(description=doc(self.__doc__),
                formatter_class=argparse.RawTextHelpFormatter,
                )
        parser.add_argument('-v', '--verbose', default=0, action='count',
                help='verbose message')
        parser.add_argument('--color', default=False, action='store_true',
                help='force print color codes')

        subparsers = parser.add_subparsers(title='action', dest='action',
                help='action')

        for cls in self.all_actions:
            act = cls()
            subparser = subparsers.add_parser(act.action, help=doc(act.__doc__))
            act.init_parser(subparser)
            subparser.set_defaults(subaction=act)

        return parser

    def main(self):
        options = self.parser.parse_args()
        LEVELS = (logging.WARNING, logging.INFO, logging.DEBUG)
        logging.basicConfig(
                level=LEVELS[min(options.verbose, len(LEVELS)-1)],
                format='%(levelname)s: %(message)s')

        global colored
        if 'output' in options:
            if not options.output.isatty() and not options.color:
                colored = lambda x, *arg, **kw: str(x)
        elif not sys.stdout.isatty() and not options.color:
            colored = lambda x, *arg, **kw: str(x)

        options.subaction.options = options

        exit(1 if options.subaction.main() is False else 0)

    class SubActionMetaClass(abc.ABCMeta):
        def __init__(cls, name, bases, attrs):
            super(cls, cls).__init__(name, bases, attrs)
            if cls.__name__ != 'SubAction':
                ActionRunner.all_actions.append(cls)

    @six.add_metaclass(SubActionMetaClass)
    class SubAction(object):
        options = None
        """options from command line arguments.
        :type: argparse.Namespace()
        """

        @abc.abstractproperty
        def action(self):
            """a string for sub-action name"""

        @abc.abstractmethod
        def init_parser(self, parser):
            """in the init process, main function will call all
            ActionRunner.init_parser to initialize subparsers"""

        @abc.abstractmethod
        def main(self):
            """main function of the sub-action"""

        def print(self, *arg, **kwarg):
            if 'file' not in kwarg and 'output' in self.options:
                kwarg['file'] = self.options.output
            try:
                print(*arg, **kwarg)
            except IOError as e:
                if e.errno == errno.EPIPE:
                    exit(0)
                raise

class ModuleNode(object):
    BUILTIN_MODULES = ('os', 're', '__future__', 'logging', 'datetime',
            'sys', 'thread', '_threading', 'threading', 'time', 'collections',
            '_threading_local', 'copy', 'json', 'simplejson', 'Crypto',
            'itertools', 'functools', '_functools', 'heapq', 'warnings',
            '__builtin__', 'urlparse', 'traceback', 'hashlib', 'inspect',
            'google.appengine', 'xml', 'third_party', 'uuid', 'random',
            'gae_mini_profiler')

    @classmethod
    def ignored_modname(cls, modname):
        for lib in cls.BUILTIN_MODULES:
            if modname.startswith(lib) and modname[len(lib):len(lib)+1] in ('.', ''):
                return True
        return False

    @classmethod
    def ignored_path(cls, path):
        if not path:
            return True
        if path.startswith('/usr/') or path.startswith('/google-cloud-sdk'):
            return True
        if path.startswith(_SRC + os.sep) \
                and path[len(_SRC) + 1].split(os.sep, 2)[0] in SKIP_DIRS:
            return True
        return False

    def __init__(self, ast_node):
        self.name = ast_node.name
        self.ast_node = ast_node
        self.imports = {}
        self.max_depth = -1

    def dump(self):
        print('<module: %s, file: %s>' % (self.name, self.ast_node.file))
        for key in self.imports:
            ast_node = self.imports[key].ast_node
            print('  %-50s [%s]' % (key, ast_node.file))

    def count_imports(self, top_level=True):
        assert self.ast_node
        self.imports = {}

        def check_and_import(ast_node):
            if isinstance(ast_node, astroid.Import):
                for name, _as_name in ast_node.names:
                    self._try_import(name, ast_node)
            if isinstance(ast_node, astroid.ImportFrom):
                modprefix = (ast_node.modname + '.') if ast_node.modname else ''
                for name, _as_name in ast_node.names:
                    if name == '*':
                        self._try_import(ast_node.modname, ast_node, level=ast_node.level)
                    else:
                        self._try_import(modprefix + name, ast_node, level=ast_node.level)
        def walk(ast_node):
            check_and_import(ast_node)
            for child in ast_node.get_children():
                walk(child)
        if top_level:
            for v in self.ast_node.locals:
                for n in self.ast_node.locals[v]:
                    check_and_import(n)
        else:
            walk(self.ast_node)
        #mnode.dump()

    def _try_import(self, modname, import_node, level=None):
        """try to import the dependent module

        :param modname: module name or symbol to import.
        :type modname: str

        :param import_node: the ast_node to the import statement
        :type import_node: astroid.Import or astroid.ImportFrom

        :param level: relative import level.
        :type level: int or None
        """
        if modname in self.imports:
            # skip if the modname is already imported by this module
            return
        if modname in ModuleLoader.seen_modules:
            # if there are a mod node in ModuleLoader, we don't need to do ast
            # parsing again.
            self.imports[modname] = ModuleLoader.seen_modules[modname]
            return

        if self.ignored_modname(modname):
            return
        try:
            dep_mod = import_node.do_import_module(modname)
        except astroid.exceptions.AstroidImportError:
            dep_mod = None

        if dep_mod is not None:
            return self._add_import(import_node, dep_mod, modname)

        if not isinstance(import_node, astroid.ImportFrom):
            return ModuleLoader.warn(import_node, 'import-error: import %s', modname)
        try:
            parent_modname, fn = os.path.splitext(modname)
            dep_mod = import_node.do_import_module(parent_modname)
            # FIXME python3 relative import??
        except:
            return ModuleLoader.warn(import_node,
                    'import-error: import from %s or %s', modname, parent_modname)
        if fn[1:] not in dep_mod.locals:
            return ModuleLoader.warn(import_node,
                    'import-error: %s from %s', fn[1:], parent_modname)
        return self._add_import(import_node, dep_mod, parent_modname)

    def _add_import(self, import_node, dep_mod, modname=None):
        if self.ignored_modname(dep_mod.name) or self.ignored_path(dep_mod.file):
            return
        if dep_mod.name != modname:
            if dep_mod.name.endswith(modname):
                ModuleLoader.warn(import_node, 'relative-import: %s (%s)', modname, dep_mod.name)
            else:
                assert dep_mod.name == modname
        self.imports[dep_mod.name] = ModuleLoader.create_node(dep_mod)

class ModuleLoader(object):
    @classmethod
    def warn(cls, ast_node, msg, *args):
        msg = '%s:%d: ' % (ast_node.root().file, ast_node.lineno) + msg
        logging.info(msg, *args)

    seen_modules = {}

    @classmethod
    def import_file(cls, fname):
        try:
            ast_node = astroid.MANAGER.ast_from_file(fname, source=True)
        except (astroid.AstroidSyntaxError, astroid.AstroidBuildingException) as e:
            logging.exception(e)
            raise
        return cls.create_node(ast_node)

    @classmethod
    def create_node(cls, ast_node):
        if ast_node.name in cls.seen_modules:
            return cls.seen_modules[ast_node.name]

        mnode = cls.seen_modules[ast_node.name] = ModuleNode(ast_node)
        return mnode

if __name__ == '__main__':
    import tempfile
    import importlib

    class IPythonRunner(ActionRunner.SubAction):
        """test import with fixed sys.path"""
        action = 'import'

        def init_parser(self, parser):
            pg = parser.add_mutually_exclusive_group()
            parser.add_argument('-i', '--ipython', default=False, action='store_true',
                    help='launch ipython after import')
            pg.add_argument('--vim', default=False, action='store_true',
                    help='launch vim for import files')
            pg.add_argument('-c', '--code', default=None,
                    help='execute code after import')
            parser.add_argument('modules', nargs='*',
                    help='modules to import')

        @staticmethod
        def replace_pyc(fname):
            if fname.endswith('.pyc'):
                return fname[0:-4] + '.py'
            return fname

        @classmethod
        def launch_ipython(cls):
            # function for ipython env, pylint: disable=unused-variable
            def open_import_file(module_name, linenum=0):
                module = importlib.import_module(module_name)
                fname = cls.replace_pyc(module.__file__)
                cmd = ['vim', '-R', fname, '+%d'%linenum]
                subprocess.call(cmd)

            import IPython
            IPython.embed()

        def main(self):
            fix_sys_path()
            logging.debug(sys.path)

            if self.options.vim:
                mod_files = []

            for modname in self.options.modules:
                mod = importlib.import_module(modname)
                if self.options.vim:
                    mod_files.append(self.replace_pyc(mod.__file__))
                else:
                    self.print('import {} -> {}'.format(modname, mod.__file__))

            if self.options.ipython:
                self.launch_ipython()
            elif self.options.vim:
                if not mod_files:
                    self.print('no module files to open')
                    return True
                cmd = ['vim', '-p'] + mod_files
                subprocess.call(cmd)
            elif self.options.code:
                exec(self.options.code)
            return True

    class PylintChecker(ActionRunner.SubAction):
        """pylint result checker"""
        action = 'pylint'

        def init_parser(self, parser):
            pg = parser.add_mutually_exclusive_group()
            pg.add_argument('--vim', default=False, action='store_true',
                    help='check and open by vim')
            pg.add_argument('-o', '--output', default=sys.stdout,
                    type=argparse.FileType('w'),
                    help='output file name')
            pg.add_argument('-s', '--summary', default=False, action='store_true',
                    help='only summary the statistics')
            parser.add_argument('-f', '--filter', default=SELECT_ALL,
                    type=lambda x: Selector(x, sep=','),
                    help='filter message by symbol, category. ex: E,F,abstract-method')
            parser.add_argument('-i', '--input', default='pylint.out',
                    type=argparse.FileType('r'),
                    help='input the pylint output')
            parser.add_argument('files', nargs='*', default=SELECT_ALL,
                    help='only show lint messages for these files')

        def main(self):
            if isinstance(self.options.files, list):
                self.options.files = Selector(self.options.files)

            def match_filter(msg):
                return msg.category in self.options.filter or \
                        msg.symbol in self.options.filter

            if self.options.vim:
                self.options.output = tempfile.NamedTemporaryFile()

            stats = PylintUtil.LintStatistics()

            has_message = False
            for msg in PylintUtil.parse(self.options.input):
                if msg.path not in self.options.files:
                    continue

                stats.scan(msg)

                if self.options.summary or match_filter(msg):
                    self.print(msg)
                    has_message = True

            if self.options.vim and has_message:
                self.options.output.flush()
                subprocess.call(['vim',
                    '+cf {}'.format(self.options.output.name),
                    '+copen'])
                self.options.output.close()
                self.options.output = sys.stdout

            return stats.summary(self.options.output)

    class DiffFiles(ActionRunner.SubAction):
        """show diff python files between HEAD and origin/master.
        only include new and modifed files, not deleted files.
        """
        action = 'diff-files'

        def init_parser(self, parser):
            parser.add_argument('-I', '--ignore', nargs='+', default=[],
                    help='ignore files with these prefix.')
            parser.add_argument('-r', '--ref', default=GitUtil.BRANCHES,
                    help='set the base branch to generate diff')

        def main(self):
            ignored = Selector(lambda x: any([x.startswith(p) for p in self.options.ignore]))
            base = GitUtil.merge_base(refs=self.options.ref)
            for line in GitUtil.diff_files(base):
                status, fname = line.split('\t', 2)
                if status == 'D':
                    continue
                if fname not in ignored and fname.endswith('.py'):
                    print(fname)

    class DiffJsFiles(ActionRunner.SubAction):
        """show diff python files between HEAD and origin/master.
        only include new and modifed files, not deleted files.
        """
        action = 'diff-js-files'

        def init_parser(self, parser):
            parser.add_argument('-I', '--ignore', nargs='+', default=[],
                    help='ignore files with these prefix.')
            parser.add_argument('-r', '--ref', default=GitUtil.BRANCHES,
                    help='set the base branch to generate diff')

        def main(self):
            ignored = Selector(lambda x: any([x.startswith(p) for p in self.options.ignore]))
            base = GitUtil.merge_base(refs=self.options.ref)
            for line in GitUtil.diff_files(base):
                status, fname = line.split('\t', 2)
                if status == 'D':
                    continue
                if fname not in ignored and (fname.endswith('.js') or fname.endswith('.jsx')):
                    print(fname)

    class DiffCoverageChecker(ActionRunner.SubAction):
        """commit coverage checker

        this command will check coverage for
        ``git diff $(git merge-base HEAD origin/master origin/staging)``
        or you could pass the diff file by ./cichecker.py coverage -i $diff
        """
        action = 'coverage'

        def init_parser(self, parser):
            parser.add_argument('-i', '--input', default=None,
                    type=argparse.FileType('r'),
                    help='change input file. set "-" for read diff from stdin')
            parser.add_argument('-r', '--ref', default=GitUtil.BRANCHES,
                    help='set the base branch to generate diff')
            parser.add_argument('--show-covered', default=False, action='store_true',
                    help='skip covered files')
            parser.add_argument('files', nargs='*', default=SELECT_ALL,
                    help='only show lint messages for these files')

        def main(self):
            if isinstance(self.options.files, list):
                self.options.files = Selector(self.options.files)

            if self.options.input is None:
                base = GitUtil.merge_base(refs=self.options.ref)
                self.options.input = GitUtil.diff(base)
                self.print('read diff from $(git diff %s)' % base)

            cov = CoverageUtil()
            missings = cov.check_diff(self.options.input, self.options.files)
            self._show_missings_table(missings)

        def _show_missings_table(self, missings):
            self.print('{:<50} | {:<19} | {}'.format(
                'file not covered' if not self.options.show_covered else 'file',
                '+++ coverage',
                '---'))
            self.print('-'*85)
            for fname, stats in sorted(list(missings.items()),
                                       key=lambda x: x[1]['add']+x[1]['rm'],
                                       reverse=True):
                has_missing = (missings[fname]['add'] or missings[fname]['rm'])
                if not self.options.show_covered and not has_missing:
                    continue
                if stats['all_add']:
                    add_lines_coverage = 100.0 * (stats['all_add'] - stats['add'])\
                                                / stats['all_add']
                else:
                    add_lines_coverage = 100.0
                self.print('{:<50} | {:>4}/{:>4} ({:6.2f}%) | {:>4}/{:>4}'.format(fname,
                    stats['all_add'] - stats['add'],
                    stats['all_add'],
                    add_lines_coverage,
                    stats['rm'],
                    stats['all_rm'],
                    ))

    class FileAnnotate(ActionRunner.SubAction):
        """annotate coverage/pylint for files"""
        action = "annotate"

        def init_parser(self, parser):
            pg = parser.add_mutually_exclusive_group()
            pg.add_argument('--vim', default=False, action='store_true',
                    help='check and open by vim')
            pg.add_argument('-o', '--output', default=sys.stdout,
                    type=argparse.FileType('w'),
                    help='output file name')
            parser.add_argument('-p', '--pylint-result', default='pylint.out',
                    help='set the pylint result file')
            parser.add_argument('-n', '--no-code', default=False, action='store_true',
                    help='do not show codes, only annotate messages')
            parser.add_argument('files', nargs='+')

        def main(self):
            lints = PylintUtil.build_index(self.options.pylint_result,
                                           Selector(self.options.files))
            cov = CoverageUtil()

            for fname in self.options.files:
                if self.options.vim:
                    self.options.output = tempfile.NamedTemporaryFile()
                    self.options.no_code = True

                self._show_annotate(fname, cov, lints)

                if self.options.vim:
                    self.options.output.flush()
                    subprocess.call(['vim',
                        fname,
                        '+set foldmethod=manual',
                        '+set scrollbind',
                        '+10vsp {}'.format(self.options.output.name),
                        '+set scrollbind',
                        ])
                    self.options.output.close()
                    self.options.output = sys.stdout

        def _show_annotate(self, fname, cov, lints):
            analysis = cov.analyze(fname)
            if not analysis:
                logging.warn('%s: no coverage result', fname)
            else:
                logging.info('%s: coverage %5.2f%%', fname, analysis.coverage())

            if not lints:
                logging.warn('%s: no pylint result', fname)
            else:
                logging.info('%s: lints count %d', fname, lints.count(fname))

            with open(fname, 'r') as source:
                for i, line in enumerate(source, start=1):
                    line = line.rstrip(os.linesep)

                    lmark = lints.lint(fname, i)
                    cmark = analysis.covered(i, line)

                    self._show_line(i, line, lmark, cmark)

        def _show_line(self, lineno, line, lint, covered):
            if self.options.no_code:
                self.print('%4d: %s%s %s' % (lineno, lint.category, covered, lint.msg))
            else:
                self.print('%4d: %s%s %s' % (lineno, lint.category, covered, line))
                if lint.category != ' ':
                    self.print('       \\ %s' % lint.msg)

    class DiffAnnotate(ActionRunner.SubAction):
        """annotate coverage/pylint for diff

        diff will be ``git diff $(git merge-base HEAD origin/master origin/staging)``
        you can use --ref or use --input to change this behavior
        """
        action = 'diff'

        def init_parser(self, parser):
            parser.add_argument('-o', '--output', default=sys.stdout,
                    type=argparse.FileType('w'),
                    help='output file name')
            parser.add_argument('-i', '--input', default=None,
                    type=argparse.FileType('r'),
                    help='input diff file, set "-" for stdin')
            parser.add_argument('-r', '--ref', default=GitUtil.BRANCHES,
                    help='set the base branch to generate diff')
            parser.add_argument('-p', '--pylint-result', default='pylint.out',
                    help='set the pylint result file')
            parser.add_argument('-s', '--summary', default=False, action='store_true',
                    help='only show summary for files')
            parser.add_argument('--skip-no-error', default=False, action='store_true',
                    help='skip files without missing (experimental) and lint error')
            parser.add_argument('files', nargs='*', default=SELECT_ALL)

        def main(self):
            if isinstance(self.options.files, list):
                self.options.files = Selector(self.options.files)

            if self.options.input is None:
                base = GitUtil.merge_base(refs=self.options.ref)
                self.options.input = GitUtil.diff(base)
                self.print('read diff from $(git diff %s)' % base)
            elif isinstance(self.options.input, file):
                # we need two pass, load into memory
                self.options.input = self.options.input.read().splitlines()

            lints = PylintUtil.build_index_by_diff(self.options.pylint_result,
                                            self.options.input,
                                           self.options.files)

            cov = CoverageUtil()
            missings = cov.check_diff(self.options.input, self.options.files)

            for diff in GitUtil.DiffParser(self.options.input, no_deleted=True):
                if diff.fname not in self.options.files:
                    continue

                if diff.fname in missings:
                    add_miss, rm_miss = (missings[diff.fname]['add'], missings[diff.fname]['rm'])
                else:
                    add_miss, rm_miss = (False, False)
                lint_errors = lints.count_error(diff.fname)

                if self.options.skip_no_error and \
                        not add_miss and not rm_miss and not lint_errors:
                    continue

                self.print('{:<70} {:>5} {:>5} {:>5}'.format(
                    colored('file: %s' % diff.fname, light=True),
                    '+' + str(add_miss) if add_miss is not False else '',
                    '-' + str(rm_miss) if rm_miss is not False else '',
                    '~' + str(lint_errors) if diff.fname in ALL_PYTHON_CODES else ''))

                if self.options.summary:
                    continue

                analysis = cov.analyze(diff.fname)

                for line in diff:
                    lmark, cmark = (' ', ' ')
                    if line.status in (line.ADDED, line.NOCHANGE):
                        cmark = analysis.covered(line)
                        lintmsg = lints.lint(diff.fname, line.lineno)
                        lmark = lintmsg.category

                        if not CoverageUtil.mark_is_covered(cmark):
                            cmark = colored(cmark, bg='red')

                    if line.status == line.DELETED:
                        self.print('    : %s%s%s%s' % (lmark, cmark, line.status,
                            colored(line.text, fg='red')))
                    else:
                        color = {}
                        if line.status == line.ADDED:
                            color['fg'] = 'green'
                        self.print('%4d: %s%s%s%s' % (line.lineno, lmark, cmark,
                            line.status, colored(line.text, **color)))
                        if lmark != ' ':
                            self.print(colored('       \\ %s' % lintmsg.msg, fg='yellow'))

    class DetectCycle(ActionRunner.SubAction):
        """detect import cycle"""
        action = 'cycle'

        def init_parser(self, parser):
            parser.add_argument('-g', '--graph', default=[], action='append',
                    help='generate graph')
            parser.add_argument('-A', '--all', default=False, action='store_true',
                    help='check all imports, not only on top-level')
            parser.add_argument('-r', '--recursive', default=False, action='store_true',
                    help='recursive check')
            parser.add_argument('files', nargs='*', default=())

        def detect_cycle(self, fname):
            mod = ModuleLoader.import_file(fname)
            trace = []
            queue = [(mod, 0)]
            seen_loop = set()
            while queue:
                mnode, depth = queue.pop()
                trace = trace[:depth]
                trace.append(mnode)

                if mnode.max_depth != -1:
                    mnode.max_depth = max(mnode.max_depth, depth)
                    continue
                else:
                    mnode.max_depth = depth

                logging.debug(colored('>'.join([x.name for x in trace]), fg='black'))
                mnode.count_imports(top_level=not self.options.all)

                for dep_name in mnode.imports:
                    dep_mod = mnode.imports[dep_name]
                    try:
                        i = trace.index(dep_mod)
                        loop = '->'.join([x.name for x in trace[i:]]) + '->' + dep_mod.name
                        if loop not in seen_loop:
                            seen_loop.add(loop)
                        logging.error('import-loop: %s%s%s',
                                '->'.join([x.name for x in trace[:i]]),
                                '->' if i != 0 else '',
                                colored(loop, fg='red'))
                        continue
                    except ValueError:
                        pass
                    queue.append((dep_mod, depth + 1))

        def generate_graph(self, fname):
            op_map = {
                    '.dot': self._generate_graph_dot,
                    '.json': self._generate_graph_json,
                    }
            ext = os.path.splitext(fname)[1]
            if ext not in op_map:
                raise RuntimeError('unknown graph type: %s' % ext)
            with open(fname, 'w') as f:
                self.options.output = f
                op_map[ext]()

        def _generate_graph_dot(self):
            self.print('digraph import_dependency {')
            self.print('\trankdir="LR";')
            self.print('\tnode [shape=box];')
            for src in ModuleLoader.seen_modules:
                mnode = ModuleLoader.seen_modules[src]
                if not mnode.imports:
                    continue
                self.print('\t"%s" -> {' % src)
                for tgr in mnode.imports:
                    self.print('\t\t"%s";' % tgr)
                self.print('\t}')
            self.print('}')
            logging.info('dot -Tsvg {input} > graph.svg # generate svg')
            logging.info('tred {input} | dot -Tsvg > graph.svg')

        def _generate_graph_json(self):
            nodes = []
            links = []
            for src in ModuleLoader.seen_modules:
                mnode = ModuleLoader.seen_modules[src]
                nodes.append({
                    'id': mnode.name,
                    'file': mnode.ast_node.file,
                    'depth': mnode.max_depth,
                    })
                for tgr in mnode.imports:
                    links.append({
                        'source': src,
                        'target': tgr,
                        })
            json.dump({'links': links, 'nodes': nodes}, self.options.output)

        def main(self):
            fix_sys_path()
            for fname in self.options.files:
                if self.options.recursive:
                    for rpath in source_walk(fname):
                        self.detect_cycle(rpath)
                else:
                    self.detect_cycle(fname)
            if self.options.graph:
                for fname in self.options.graph:
                    self.generate_graph(fname)

    class ImportAll(ActionRunner.SubAction):
        """import all modules...."""
        action="importall"
        def init_parser(self, parser):
            parser.add_argument('--show', default=False, action='store_true',
                    help='show info')
            parser.add_argument('-p', '--progress-file', default='import.progress.json',
                    help='progress file name')
            parser.add_argument('root', default=_SRC,
                    help='traverse root for trying import all python codes')

        @staticmethod
        def parse_modname(fname):
            if fname.startswith('./'):
                fname = fname[2:]
            fname = os.path.splitext(fname)[0]
            mod = os.path.basename(fname)
            if mod == '__init__':
                fname = os.path.dirname(fname)
            return fname.replace('/', '.')

        def _open_progress(self):
            try:
                with open(self.options.progress_file) as f:
                    progress = json.load(f)
            except IOError:
                progress = {}
            return progress

        def _save_progress(self, progress):
            with open(self.options.progress_file, 'w') as f:
                json.dump(progress, f, indent=2)

        def show_progress(self):
            progress = self._open_progress()
            for modname, info in list(progress.items()):
                print('{modname}: {file} {import}'.format(**info))
            #self._save_progress(progress)

        def scan_codes(self, fname, progress):
            modname = self.parse_modname(fname)
            if modname in progress:
                return True

            info = dict(file=fname, modname=modname)
            try:
                logging.info('try to import %s in subprocess' % modname)
                subprocess.check_call(['python', __file__, 'import', modname])
                info['import'] = True
                progress[modname] = info
            except subprocess.CalledProcessError as e:
                logging.warn('import %s failed' % modname)
                info['import'] = False
                progress[modname] = info
                return False
            except KeyboardInterrupt as e:
                return False

            self._save_progress(progress)
            return True

        def main(self):
            if self.options.show:
                self.show_progress()
                return
            progress = self._open_progress()
            for rpath in source_walk(self.options.root):
                if os.path.basename(rpath) == '__main__.py':
                    continue
                if not self.scan_codes(rpath, progress):
                    break

    class GenerateApi(ActionRunner.SubAction):
        """generate api doc template"""
        action = "genapi"
        def init_parser(self, parser):
            parser.add_argument('files', nargs='*', default=())

        def gen_api(self, mod):
            def walk(ast_node):
                if isinstance(ast_node, astroid.Class) \
                        and ast_node.is_subtype_of('google.appengine.ext.db.Model'):
                    print('  - schema:')
                    print('        id: ' + ast_node.name)
                    print('        properties:')
                    for k, v in list(ast_node.locals.items()):
                        if len(v) != 1:
                            continue
                        if isinstance(v[0], astroid.AssignName):
                            try:
                                print('            %s:' % v[0].name)
                                type_name = v[0].assign_type().as_string().split('=')[1].strip().split('(')[0]
                                print('                type: %s' % type_name)
                            except:
                                continue
                for child in ast_node.get_children():
                    walk(child)
            walk(mod.ast_node)

        def main(self):
            fix_sys_path()
            for fname in self.options.files:
                mod = ModuleLoader.import_file(fname)
                self.gen_api(mod)

    class Checker(ActionRunner):
        """
        check the pylint,coverage output for CI

        example usages:

            {src} import google.appengine.ext.db
            {src} pylint --input pylint.out -f F,E,broad-except
            {src} coverage
            git diff origin/master | {src} coverage -i - --show-covered
            {src} annotate main.py
            {src} diff
        """

    Checker().main()
