# -*- coding: utf-8 -*-
import auth.user_util
import content.topic
import instance_cache


def get_topic_page_json_impl(topic_id):
    current_user = auth.user_util.User.current()
    return content.topic.get_topic_page(topic_id=topic_id,
                                        include_hidden=current_user.is_moderator if current_user else False)


def validate_or_update_topic_by_edu_sheet(topic_id, title, description, validation_only, version_id="edit"):
    if version_id != "edit":
        return {
            'topic_id': topic_id,
            'errors': ['目前禁止於 edit 以外的版本修改 Topic']
        }

    err_response, updated_topic = content.topic.update_topic(topic_id, title, description, validation_only, version_id)
    return {
        'topic_id': topic_id,
        'errors': err_response
    }


def validate_or_create_topic_by_edu_sheet(topic_id, title, description, parent_id, validation_only, version_id="edit"):
    if version_id != "edit":
        return {
            'topic_id': topic_id,
            'errors': ['目前禁止於 edit 以外的版本建立 Topic']
        }

    err_response, created_topic = content.topic.create_topic(topic_id, title, description, parent_id, validation_only, version_id)
    return {
        'topic_id': topic_id,
        'errors': err_response
    }


def flush_instance_cache(category):
    if instance_cache._debug:
        instance_cache.flush(category=category)
        return 'ok'
    return 'fail'


def warm_up():
    auth.user_util.get_cached_developer_list()
    auth.user_util.get_cached_moderator_list()
    return '', 200, {}
