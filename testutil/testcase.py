# -*- coding: utf-8 -*-
import logging
import unittest
import api.request_cache
import instance_cache


class NoCacheTestCase(unittest.TestCase):
    def setUp(self):
        api.request_cache.flush()
        instance_cache.flush()



