# -*- coding: utf-8 -*-
import logging
import os

import google.cloud.logging
from flask import Flask
from flask import request, jsonify, abort
from flask_cors import CORS
from google.cloud.logging.handlers import AppEngineHandler, setup_logging

import api_implement
import content.exception
import content.topic
from api import wsgi_compat, request_cache, api_util
from auth.decorators import moderator_required

app = Flask(__name__)
CORS(app)
app.wsgi_app = wsgi_compat.WSGICompatHeaderMiddleware(app.wsgi_app)


def init_logging():
    # Instantiates a client
    client = google.cloud.logging.Client()
    handler = AppEngineHandler(client, name='stdout')
    logging.getLogger().setLevel(logging.DEBUG)
    setup_logging(handler)
    logging.info('logging initialized')


def get_request_data(data, key, default_val=None):
    if key in data:
        return data[key]
    return default_val


def trim_string(src, strip_crlf=True):
    if src:
        src = src.strip()
        if strip_crlf:
            src = src.replace('\r', '').replace('\n', '')
    return src


@app.before_request
def clean_request_cache():
    request_cache.flush()


XSRF_COOKIE_KEY = "fkey"
XSRF_HEADER_KEY = "X-KA-FKEY"


@app.before_request
def check_xsrf_value():
    cookie_value = request.cookies.get(XSRF_COOKIE_KEY)
    header_value = request.headers.get(XSRF_HEADER_KEY)
    if not cookie_value or not header_value:
        abort(400)
    if cookie_value != header_value:
        logging.warning('Mismatch xsrf value, possible from hacker')
        abort(403)


def check_required_args_exist(*args):
    """
    GET parameter parser
    """
    ret = []
    for required_param in args:
        param_name = required_param[0]
        cast_fn = required_param[1]
        if param_name not in request.args:
            raise ValueError("Require [%s] GET param" % param_name)
        ret.append(cast_fn(request.args[param_name]))
    return ret


@app.route('/api/content/hello')
def hello():
    """Return a friendly HTTP greeting."""
    logging.error('e: here')
    logging.warning('w: here')
    logging.info('i: here')
    logging.debug('d: here')
    return 'Hello World!'


@app.route('/api/content/topicpage')
def topicpage():
    """
    Retrieve the listing of subtopics and videos for this content.
        Used on the content page. e.g. /junyi-math/m1s/mesfl
    """
    try:
        checked_args = check_required_args_exist(('topic_id', str))
        topic_id = checked_args[0]
    except ValueError as e:
        return api_util.api_invalid_param_response(str(e))

    try:
        resp = api_implement.get_topic_page_json_impl(topic_id)
    except content.exception.ContentNotExistsError as e:
        return api_util.api_invalid_param_response(str(e))

    return jsonify(resp)


@app.route("/api/content/topictree/<version_id>/edu_sheet/topic/update", methods=["PUT"])
@moderator_required
def validate_or_update_topic_by_edu_sheet(version_id="edit"):
    if version_id != "edit":
        return api_util.api_invalid_param_response("目前禁止於 edit 以外的版本建立 Topic")
    request_json = request.get_json()
    topic_id = trim_string(get_request_data(request_json, 'topic_id', ''))
    topic_title = trim_string(get_request_data(request_json, 'topic_title', ''))
    description = trim_string(get_request_data(request_json, 'description', ''), False)
    validation_only = bool(int(get_request_data(request.args, 'validation_only', True)))
    return api_implement.validate_or_update_topic_by_edu_sheet(topic_id, topic_title, description, validation_only,
                                                               version_id)


@app.route("/api/content/topictree/<version_id>/edu_sheet/topic/create", methods=["POST"])
@moderator_required
def validate_or_create_topic_by_edu_sheet(version_id="edit"):
    if version_id != "edit":
        return api_util.api_invalid_param_response("目前禁止於 edit 以外的版本建立 Topic")
    request_json = request.get_json()
    topic_id = trim_string(get_request_data(request_json, 'topic_id', ''))
    topic_title = trim_string(get_request_data(request_json, 'topic_title', ''))
    description = trim_string(get_request_data(request_json, 'description', ''), False)
    parent_id = trim_string(get_request_data(request_json, 'parent_id', ''))
    validation_only = bool(int(get_request_data(request.args, 'validation_only', True)))
    return jsonify(api_implement.validate_or_create_topic_by_edu_sheet(
        topic_id, topic_title, description, parent_id, validation_only, version_id))


# ths same with @route("/api/v1/topicversion/<version_id>/topictreedata") in junyiacademy
# ths same with @route("/api/v1/topictreedata") in junyiacademy
@app.route("/api/content/topictreedata", methods=["GET"])
def get_topic_tree_data():
    """
    Retrieve the listing of subtopics and videos for this topic. Used on the topic page.
    ---
    id:
      - topic
    tags:
      - Content
    parameters:
      - in: query
        name: version_id
        required: false
        type: string
        description: 可以接受以下三種格式：default/edit/版本號碼
    responses:
        200:
            description: 已成功抓取 topic tree 資料
            schema:
                $ref: '#/definitions/Topic'
        400:
            description: 找不到 version_id
    """
    if 'casing' not in request.args or request.args['casing'] != 'camel':
        logging.error('Error: Not support other casing!')
        return api_util.api_invalid_param_response('Error: Not support other casing!')

    version_id = request.args["topicversion"] if "topicversion" in request.args else None
    try:
        resp = content.topic.get_topic_tree_data(version_id=version_id)
    except content.exception.ContentNotExistsError as e:
        return api_util.api_invalid_param_response("Could not find content data of version %s [%s]"
                                                   % (version_id, e))
    return jsonify(resp)


@app.route('/api/content/instance_cache/flush')
def flush():
    if 'category' not in request.args:
        return 'arg error'
    return api_implement.flush_instance_cache(request.args['category'])


@app.route('/_ah/warmup')
def warm_up():
    # init_logging()
    return '', 200, {}


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    logging.basicConfig(level=logging.DEBUG)
    app.run(host='0.0.0.0', port=8010, debug=True)

if os.getenv('GAE_VERSION'):
    init_logging()
